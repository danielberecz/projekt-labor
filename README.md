# Software Project Lab Assignment #

## Goal
- Create an object-oriented program in Java using UML and RUP methodology

## About the game
- O'Neill and Jaffa have to collect the ZPMs on the map
- Avoid pits and replicators
- Create Portal like wormholes
- Open gates using switches and boxes

## Compile
- Requirements: Java SE Development Kit 8
- In windows:
- md bin
- javac -d bin ./src/game/*.java ./src/fieldelements/*.java ./src/movables/*.java

## Run 
- java game.Main <mapFile>
- see examples for mapFile in /maps

## Tests
- Run command in /bin:
- for /f %f in (’dir /b ..\INPUT\’) do java game.Main ..\MAP\%f < ..\INPUT\%f >OUTPUT\%f
- for /f %f in (’dir /b OUTPUT\’) do FC OUTPUT\%f EXPECTED\%f > DIFFERENT\%f

# Team z4qqqbatmanjel #
- Berecz Dániel
- László Dániel
- Lauer János
- Sümegi Márk
- Szabó Attila Dániel