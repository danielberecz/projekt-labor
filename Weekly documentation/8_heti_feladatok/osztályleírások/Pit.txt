\item Pit
A játékban szereplõ szakadékok megvalósítása. Felelõssége, hogy ha a dobozzal vagy O'Neill ezredessel interakcióba kerül, akkor megsemmisítse õket. A lövedék elrepül felette.

\item Ősosztályok:
FieldElement

\item Interfészek:
Nincs

\item Attribútumok:
-ownField:Basicfield: az õsosztályból örökölt tagváltozó.
 
\item Metódusok:
+void meet(box: Box): A dobozzal való interakció függvénye, a dobozt megsemmisíti.
+void meet(s: Soldier): O'Neill ezredessel/Jaffával való interakció függvénye, megsemmisíti aki rálép.
+void meet(bullet: Bullet): Ez a függvény szól a lövedéknek, hogy oda mozoghat az adott Fieldre.
+void meet(r: Replicator): A replikátorral valo interakció függvénye, megsemmisíti a replikátort és a szakadékot is.