Door
A játékban szereplõ ajtók megvalósításáért felelõs osztály, mely egy pályaelem. Feladata, hogyha a hozzákapcsolt mérleget lenyomják, akkor kinyíljon, ellenkezõ esetben legyen zárva. Ha O'Neil egy nyitott ajtó elõtt a hozzátartozó mérlegrõl akar átugrani, a záródó ajtó megöli.

\item Ősosztályok:
FieldElement

\item Interfészek
nincs

\item Attribútumok
-mySwitch: Switch: a hozzá kapcsolt mérleg.
-state: Accessibility: az ajtó két állapotot vehet fel: open/closed

\item Metódusok
+open(): void: Az ajtó kinyitásáért felelõs metódus.
+close(): void: Ezzel a metódussal lehet az ajtót becsukni.
