\item Game
A protoban megfogalmazott nyelv feldolgozásáért, a parancsok értelmezéséért és végrehajtásáért felelős osztály. Ő hozza létre a pályához szükséges elemeket, és rakja a játékosokat a kezdőpozícióba. Ez az osztály felelős az új ZPM-ek létrehozásáért. 

item Ősosztályok
Nincs

\item Interfészek:
Nincs

\item Attribútumok:
	\begin{itemize}
		\item[] - labyrinth: List<BasicField>: a pályához tartozó mezők
		\item[] - ZPMcounter: Map<Soldier, int>: számontartja, hogy az egyes \textit{Soldier}-ek eddig hány ZPM-et gyüjtöttek össze
	\end{itemize}

\item Metódusok:
	\being{itemize}
		 \item[] + ZPMCollected(s: Soldier): void: Rögzíti, hogy egy adott \textit{Soldiern} ZPM-t vett fel.
		 \item[] + createZPM(): void: Feltétel teljesülésekor (páros számű ZPM lett egy \textit{Soldier}-nél) létrehoz egy új ZPM-et.
	\end{itemize}

