A programot parancssorból futtathatjuk. Nyissunk meg egy parancssort.
A PATH környezeti változónak tartalmaznia kell a Java könyvtárát. Ehhez lásd \ref{ssec:forditas}-t.
Navigáljunk el a lefordított állományok mappájába (lásd \ref{ssec:forditas}). Pl.: Ha a lefordított állományok a 
C:/Users/123456/Documents/projlab/z4qqqbatmanjel/bin mappában vannak, akkor ajduk ki a következő parancsot:

\lstset{escapeinside=`', xleftmargin=10pt, frame=single, basicstyle=\ttfamily\footnotesize, language=sh}
\begin{lstlisting}
	cd C:/Users/123456/Documents/projlab/z4qqqbatmanjel/bin
\end{lstlisting}

Ezután a következő paranccsal futtathatjuk a programot:
\lstset{escapeinside=`', xleftmargin=10pt, frame=single, basicstyle=\ttfamily\footnotesize, language=sh}
\begin{lstlisting}
	java oneill.Main
\end{lstlisting}
