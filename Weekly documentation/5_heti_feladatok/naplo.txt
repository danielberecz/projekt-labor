\bejegyzes{2016.03.16. 12:00}{1 óra}{Berecz, László, Sümegi, Szabó}{Konzultáció}
\bejegyzes{2016.03.16. 19:00}{0.5 óra}{Berecz}{Use-case-ek definiálása }
\bejegyzes{2016.03.16. 20:00}{0.5 óra}{Berecz}{Feladatok kiosztása: Sümegi --> Logger osztály megtervezése, Szabó --> Bullettel és Dobozzal kapcsolatos Use-Case-ek, Lauer --> Soldierrel kapcsolatos Use-Case-ek, László --> Az elkészült szekvencia és kommunikációs diagramok ellenőrzése, Berecz --> Use-case diagram, Menü}
\bejegyzes{2016.03.18. 16:00}{2.0 óra}{Szabó}{kezdetleges szekvencia és kommdiag. készítése}
\bejegyzes{2016.03.19. 15:00}{3 óra}{Sümegi}{Logger osztály tervezése}
\bejegyzes{2016.03.19. 16:00}{1.5 óra}{Lauer}{Usa-Case leírások elkészítése}
\bejegyzes{2016.03.19. 23:00}{1 óra}{Berecz}{Use-Case diagram}
\bejegyzes{2016.03.20. 00:00}{4 óra}{Berecz}{Formázás}
\bejegyzes{2016.03.20. 18:00}{5 óra}{Lauer}{Use-Case-ek szekvencia és kommunikációs diagramjainak készítése}
\bejegyzes{2016.03.20. 21:00}{0.5 óra}{Berecz}{Menü elkészítése}
\bejegyzes{2016.03.20. 21:30}{2.5 óra}{Szabó}{Újragondolt szkeletonhoz megfelelő diagramok}
\bejegyzes{2016.03.21. 00:30}{1.5 óra}{Szabó}{Hibák javítása}
\bejegyzes{2016.03.21. 00:00}{0.5 óra}{Berecz}{Napló elkészítése}
\bejegyzes{2016.03.21. 00:30}{1 óra}{Berecz}{Use-Case leírások elkészítése}
\bejegyzes{2016.03.21. 01:00}{1 óra}{Berecz}{Use-Case-ek szekvencia és kommunikációs diagramjainak készítése}
\bejegyzes{2016.03.21. 02:00}{1 óra}{Berecz}{Formázás véglegesítése}