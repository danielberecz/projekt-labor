\item Felelősség

A játékban szereplõ ZPM-ek megvalósításáért felelõs osztály. Ennek az osztálynak az a felelõssége, hogy ha O'Neill ezredessel kerül kapcsolatba, akkor az ezredes fel tudja venni a ZPM-et. 
\item Õsosztályok:

FieldElement
\item Interfészek:

nincs
\item Attribútumok: 
	\begin{itemize}
		\item[] - ownField: BasicField: Az õsosztályból örökölt tagváltozó. 
		\item[] -g: Game: Az új ZPM létrehozásához szükséges
	\end{itemize}


Metódusok:
	\begin{itemize}
		\item[] + meet(s: Soldier): void: Beérkezõ O'Neill ezredessel történõ interakció függvénye. O'Neill felveszi a ZPM-et. 
	\end{itemize}


