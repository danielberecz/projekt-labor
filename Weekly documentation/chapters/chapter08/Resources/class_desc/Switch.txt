\item Felelősség


A játékban szereplõ mérlegek megvalósításáért felelõs osztály. Ezzel lehet a hozzá kötött ajtó állapotát változtatni, azaz ha lenyomódik akár doboz, akár O'Neill ezredes hatására, akkor az ajtó kinyílik, ellenkezõ esetben a megfelelõ ajtó zárva kell, hogy legyen.
\item Õsosztályok:

FieldElement
\item Interfészek

nincs
\item Attribútumok
	\begin{itemize}
		\item[] - myDoor: Door: A hozzá kapcsolt ajtó.
		\item[] - minWeightToOpen: int: az ajtó kinyílásához szükséges minimum súly
		\item[] - weightOnMe: int : az aktuálisan a mérlegen lévő súly
	\end{itemize}

Metódusok
	\begin{itemize}
		\item[] + press(w: int): void: A mérleg adott súllyal való megnyomásáért felelõs metódus.
		\item[] + release(w: int): void: A mérlegről adott súllyal való leszállásért felelős metódus.
	\end{itemize}
