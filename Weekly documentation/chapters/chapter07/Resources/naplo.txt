\bejegyzes{2016.03.30. 12:00}{1.5 óra}{Berecz, Lauer, László, Sümegi, Szabó}{Konzultáció, Tesztelés}
\bejegyzes{2016.03.31. 19:00}{1 óra}{Berecz, Lauer, László, Sümegi, Szabó}{Feladatok kiosztása:Sümegi --> nyelv elkészítése, Berecz, László --> Use-Case-k definiálása, változtatások realizálása, Lauer, Szabó --> tesztesetek megírása}
\bejegyzes{2016.04.02. 10:00}{6 óra}{Sümegi}{Nyelv definiálása}
\bejegyzes{2016.04.02. 18:00}{1 óra}{Berecz}{Use-Casek felvétele}
\bejegyzes{2016.04.02. 19:00}{4 óra}{László}{Osztálydiagram bővítése, szekvencia diagramok frissítése}
\bejegyzes{2016.04.03. 11:00}{4 óra}{Lauer}{Tesztek készítése}
\bejegyzes{2016.04.03. 11:00}{2 óra}{Szabó}{Tesztek készítése}
\bejegyzes{2016.04.03. 15:00}{2 óra}{Berecz}{Új szekvenciák megtervezése}
\bejegyzes{2016.04.03. 20:00}{3 óra}{Szabó}{Tesztek készítése}
\bejegyzes{2016.04.04. 08:00}{0.5 óra}{Berecz}{Napló készítése}
\bejegyzes{2016.04.04. 00:30}{3 óra}{Berecz, László}{Formázás}