



\begin{tabular}{p{3cm} p{12cm}}
Pálya fileok & A háttértáron található adat fileok, amik a labirintus következő tulajdonságait tartalmazzák: \\
& ~~\llap{\textbullet}~~ falak elhelyezkedése és színe   \\
& ~~\llap{\textbullet}~~ dobozok, szakadékok és ZPM modulok labirintuson belüli helye.  \\




Pályák & A háttértárból betöltött pálya adatok halmaza, ezeken a pályákon lehet játszani a program elindítása után. Ez az egység szolgáltatja a szabályos működéshez szükséges adatokat a Játékmotornak. \\



Játékmotor & A program központi vezérlőegysége. A játékmotor valósítja meg a program logikáját, vagyis kiszámolja a játékbeli szereplők (a játékos és lövedékek) mozgását, ügyel a játékos és a labirintus falának ütközésére, lehetővé teszi a tárgyak felszedését, számon tartja a megszerzett ZPM modulok számát, és figyeli a játék végét jelentő feltételeket.\\

Grafikus felület & A játék megjelenítésre szolgáló egység. Input-Output interfaceként működik a felhasználó és a játékmotor között.\\


\end{tabular}

A software használata nem igényel hálózatot.

