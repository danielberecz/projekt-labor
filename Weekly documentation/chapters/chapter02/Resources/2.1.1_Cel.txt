A projekt teljes fejlesztését leíró és támogató dokumetum létrehozása, mely a specifikációtól kezdve a tervezés fáziasan keresztül a végleges grafikai verzió részleteit is tartalmazza. A teljes anyagból bárki megértheti a készített játék működését.

