\usecase{Pálya kiválasztás}
{A felhasználó kiválaszt egy pályát.}
{Felhasználó}
{A program felsorolja a lehetségesen betölthető pályákat, majd a felhasználó ezek közül választ egyet.}