\usecase{Újrakezdés}
{A felhasználó újrakezdi az adott pályát.}
{Felhasználó}
{A felhasználó újrakezdi a pályat, azaz az adott pálya újra betöltődik az eredeti állapotának megfelelően.}