\usecase{Kilépni}
{Kilépni az adott pályáról.}
{Felhasználó}
{Aktív játék közben (tehát az ezredes még él) kilépni az adott pályáról annak teljesítése nélkül.}