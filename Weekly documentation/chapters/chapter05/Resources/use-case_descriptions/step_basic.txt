\usecase{Sima mezõre lépés}
{Az ezredes egy sima mezõre lép, amin nincs más objektum.}
{Felhasználó}
{Az ezredes tovább halad a pályán, egy sima mezõre lép, ahol nem lép más objektummal interakcióba.}