A programot parancssorból futtathatjuk. Nyissunk meg egy parancssort.
A PATH környezeti változónak tartalmaznia kell a Java könyvtárát. Ehhez lásd \ref{ssec:forditas}-t.
Navigáljunk el a lefordított állományok mappájába (lásd \ref{ssec:forditas}). Pl.: Ha a lefordított állományok a 
C:\textbackslash Users\textbackslash 123456\textbackslash Documents\textbackslash projlab\textbackslash z4qqqbatman

jel\textbackslash bin mappában vannak, akkor ajduk ki a következő parancsot:

\lstset{escapeinside=`', xleftmargin=10pt, frame=single, basicstyle=\ttfamily\footnotesize, language=sh}
\begin{lstlisting}
	cd C:/Users/123456/Documents/projlab/z4qqqbatmanjel/bin
\end{lstlisting}

Ezután a következő paranccsal futtathatjuk a programot:
\lstset{escapeinside=`', xleftmargin=10pt, frame=single, basicstyle=\ttfamily\footnotesize, language=sh}
\begin{lstlisting}
	java game.Main mapFile.txt
\end{lstlisting}

Ahol a mapFile.txt egy tetszöleges labirintus-leíró fájl.


Az összes teszt lefuttatásást a következő parancs teszi lehetővé, amelyet a bin könyvtárban kell kiadni:
\lstset{escapeinside=`', xleftmargin=10pt, frame=single, basicstyle=\ttfamily\footnotesize, language=sh}
\begin{lstlisting}
	for /f %f in ('dir /b ..\INPUT\') do java game.Main ..\MAP\%f 
	< ..\INPUT\%f >OUTPUT\%f
\end{lstlisting}

Az összes kimenet összehasonlítását és a különbségek fájlba írását, a következő parancs teszi lehetővé:
\lstset{escapeinside=`', xleftmargin=10pt, frame=single, basicstyle=\ttfamily\footnotesize, language=sh}
\begin{lstlisting}
	for /f %f in ('dir /b OUTPUT\') do FC OUTPUT\%f EXPECTED\%f > DIFFERENT\%f
\end{lstlisting}