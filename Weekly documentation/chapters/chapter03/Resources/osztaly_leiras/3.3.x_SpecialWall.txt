\item Felelőség
A játékban szereplõ falak egy típusa, melynek felelõssége az, hogy ha lövedék vele interakcióba lép, akkor ott csillagkaput hozzon létre. 
\item Õszosztályok:

FieldElement $\rightarrow$ Wall
\item Interfészek:

Nincs
\item Attribútumok:
	\begin{itemize}
		\item[] - ownField: BasicField: Örökölt tagváltózó az õsosztályból. 
		\item[] - wormhole: Wormhole: Egy féregjárat, melybõl mindig maximum csak egy létezik.(Statikus attribútum)
	\end{itemize}	
\item Metódusok: 
	\begin{itemize}
		\item[] + visit(bullet:Bullet): void: Lövedékkel való interakcióba lépés lekezelõ függvény. Frissíti a féregjárat statikus tagváltozóját. 
		\item[] + visit(soldier:Soldier): void: Az ezredessel interakcióba lépés esetén hívódik meg.
	\end{itemize}