\item Felelőség


A játékban szereplõ mérlegek megvalósításáért felelõs osztály. Ezzel lehet a hozzá kötött ajtó állapotát változtatni, azaz ha lenyomódik akár doboz, akár O'Neill ezredes hatására, akkor az ajtó kinyílik, ellenkezõ esetben a megfelelõ ajtó zárva kell, hogy legyen.
\item Õsosztályok:

FieldElement
\item Interfészek

nincs
\item Attribútumok
	\begin{itemize}
		\item[] - myDoor: Door: A hozzá kapcsolt ajtó.
		\item[] - state: SwitchState: A mérleg két állapotot vehet fel: pressed/relaxed.
	\end{itemize}

Metódusok
	\begin{itemize}
		\item[] + press(): void: A mérleg megnyomásáért felelõs metódus.
	\end{itemize}
