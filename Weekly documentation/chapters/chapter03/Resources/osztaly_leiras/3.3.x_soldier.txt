\item Soldier

Oneil ezredest megvalosító osztály. Ez az osztály biztosítja a legtöbb felhasználó által végrehajtható funkció (lőni, dobozt felvenni/lerakni, ZPM-t felvenni)
\item Ősosztály

Movable  $\rightarrow$ SelfMovable
\item Interfészek

nincs
\item Attribútumok
	\begin{itemize}
		\item[] - currentField: BasicField Az ősosztályból örökölt tagváltozó.
		\item[] - facing: Direction Az ősosztályból örökölt tagváltozó.
		\item[] - carriedBox: Box Ebben a változóban tároljuk az ezredesnél lévő dobozt, ha van.
	\end{itemize}	
\item Metódusok
	\begin{itemize}
		\item[] + accept(m: Movable): void: Örökölt függvény, a visitor patternek megfelelően meghívja a bejövő paraméter visit függvényét önmagával, hogy kapcsolatba léphessen a mezőn lévő Movable-ből származő objektumokkal.
		\item[] + accept(bf:BasicField): void: Örökölt függvény, a visitor patternek megfelelően meghívja a bejövő paraméter visit függvényét önmagával, hogy kapcsolatba léphessen a mezőn lévő FieldElementből származó objektumokkal.
		\item[] + interact(b:Box): void: Örökölt függvény Az ezredes és egy mezőn lévő (tehát még nem felvett) doboz közötti interakciót kezeli le. 
		\item[] + destroy(): void: Örökölt függvény, az ezredes halálával véget ér a játék, ezt jelzi a felhasználónak.
		\item[] + shoot(c:Colour): void: Ez a függvény felel a lövésért (lövedék létrehozása). A bemeneti paramétere egy enum (Colour: orange/blue), amely meghatározza, hogy milyen színű lövedéket löjjön ki.
		\item[] + pickUp(zpm:ZPM): void: Ez a függvény felel azért, hogy az ezredes felvegye a ZPM-eket a játék során.
		\item[] + pickUpBox(box:Box): void: Ez a függvény felel azért, hogy az ezredessel fel tudjunk venni dobozokat, ha szeretnénk.
		\item[] + putBox(bf:BasicField): void: Ez a fügvény felel azért, hogy egy dobozt le tudjunk rakni egy adott (bemeneti paraméter) mezőre.
	\end{itemize}