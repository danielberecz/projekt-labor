A speciális fal az egyetlen olyan elem a játékban amin csillagkapu nyitható. A játékos szempontjából ugyan úgy működik mint a normál fal, vagyis a játékos nem tartózkodhat vele egy mezőn. A lövedék viszont becsapódáskor (a megsemmisülés után) egy a lövedék színével megegyező csillagkaput nyit a falon. 

Felelősség: Megakadályozza, hogy a Játékos vele egy mezőre lépjen, a vele egy mezőre kerülő lövedékeket megsemmisíti, és a lövedék színének megfelelő csillagkapunak ad helyet.
