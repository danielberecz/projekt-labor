\item Felelőség

Egy absztrakt õsosztály, melynek szerepe, hogy meet függvényeket nyújtson az öröklõknek, illetve a tényt, hogy melyik mezõn vannak.
\item Õsosztályok:

nincs
\item Interfészek

nincs
\item Attribútumok
	\begin{itemize}
		\item [] \# ownField: BasicField: Tárolja, hogy melyik mezõn van az objektum.
	\end{itemize}

\item Metódusok
	\begin{itemize}
		\item [] + meet(box: Box): void: A beérkezõ dobozzal történõ interakció absztrakt függvénye.
		\item [] + meet(soldier: Soldier): void: A beérkezõ O'Neill ezredessel történõ interakció absztrakt függvénye.
		\item [] + meet(bullet: Bullet): void: A beérkezõ golyóval történõ interakció absztrakt függvénye.
		\item [] + leave(m: Movable): void: Az O'Neil által elhagyott mezõ frissítésére szolgál, pl.: ha lelép egy mérlegrõl és nem hagyott maga után dobozt, akkor annak vissza kell állnia alap pozíciójába, a hozzá kötött ajtóval együtt.
	\end{itemize}
