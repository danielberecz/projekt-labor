\item Felelőség

A Movable osztály egy absztakt ősosztály, ebből származik az összes olyan osztály, amely valamiféle mozgást tud végre hajtani. A reverse visitor pattern szerint ezt az osztályt lehet meglátogatni. 
\item Ősosztályok

nincs
\item Interfészek

nincs
\item Attribútumok
	\begin{itemize}
		\item[] \# currentField: BasicField: Ebben változóban tároljuk, hogy jelenleg melyik mezőn tartózkodik az adott objektum.
		\item[] \# weight: int: Objektum súlya
	\end{itemize}

\item Metódusok
	\begin{itemize}
		\item[] + getWeight(): Weight: A \textit{weight} tagváltozó értékét adja vissza.
		\item[] + accept(m: Movable):void A \textit{Movable} ősosztállyal rendelkező osztályok is kapcsolatba lépnek egymással, és erre az interakcióra is a visitor patternt használtuk
		\item[] + accept(fe:FieldElement):void A reverse visitor patternhez tartozó \textit{accept} függvény, ez a funkció biztosítja a mozgást és az ezzel járó interakciókat
		\item[] + interact(s:Soldier):void A reverse visitor pattern kiegészítése: a \textit{Movable} leszármazottjainak biztosít felületet a \textit{Soldier} osztállyal (Oneil) való interakcióra
		\item[] + interact(b:Box):void A reverse visitor pattern kiegészítése: a \textit{Movable} leszármazottjainak biztosít felületet a \textit{Box} osztállyal való interakcióra
		\item[] + interact(b:Bullet):void A reverse visitor pattern kiegészítése: a \textit{Movable} leszármazottjainak biztosít felületet a \textit{Bullet} osztállyal való interakcióra
		\item[] + move(bf:BasicField):void A \textit{Movable}-ből származó osztályok mozgását teszi lehetővé
		\item[] + destroy():void Az objektum megsemmisülését kezeli le
	\end{itemize}
