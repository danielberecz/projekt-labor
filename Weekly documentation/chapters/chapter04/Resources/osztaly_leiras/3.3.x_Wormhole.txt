\item Felelőség


Speciális falon a lövedékek becsapódásának hatására létrejövõ féregjáratot megvalósító osztály. Azért is felelõs, hogy ha új lövedéket lõnek speciális falra, akkor a legrégebbi azonos színû féreglyukat eltûntesse és az újjal nyisson új féregjáratot. Önmagában nem létezhez, csak egy speciális fal elemeként.
\item Õsosztályok:

nincs
\item Interfészek:

nincs
\item Attribútumok:
	\begin{itemize}
		\item[] + gates: Map<Colour, Stargate>: A csillagkapukat tárolja a változó szín - csillagkapu értékpárokba rendezve.
	\end{itemize}

\item Metódusok:
	\begin{itemize}
		\item[] + openStargate(colour: Colour, stargate: Stargate): void: Adott színû csillagkapu létrehozása, rögzítése a \textit{gates} változóba. 
		\item[] + travelThrough(O'Neill: Soldier, startingGateColour: Colour): void: Féregjáraton való áthaladást biztosító függvény. 
	\end{itemize}