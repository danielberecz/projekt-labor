package movables;

import fieldelements.BasicField;
import game.Logger;
import game.Main.Direction;

public class SelfMovable extends Movable {

    protected Direction facing;
    
    public SelfMovable() {
        facing = Direction.NORTH;
    }

    public Direction getDirection () {
        return facing;
    }

    public void startMoving() {
        if (currentField != null) {
            currentField.tryMoving(this, facing);
        }
    }
    
    public void rotate (Direction d)
    {
        if (currentField != null) {
            facing = d;
            Logger.log(name + " turned: " + d.toString());
        }

    }

    @Override
    public void destroy() {
        currentField.forget(this);
        currentField = null;
        Logger.log(name + " died");
    }

    @Override
    public void move(BasicField bf) {
        bf.memorize(this);
        currentField.forget(this);
        currentField = bf;

        int x = currentField.getX();
        int y = currentField.getY();
        Logger.log(name + " moved: " + facing.toString() + " (" + x +", " + y + ")");
    }
}
