package movables;

import fieldelements.BasicField;
import fieldelements.FieldElement;
import game.Logger;

public class Box extends Movable {

    private static int id = 1;

    public Box(BasicField bf, int weight) {
        this.weight = weight;
        currentField = bf;
        name = "Box" + id++;
        
        currentField.memorize(this);
    }

    @Override
    public void destroy() {
        currentField.forget(this);
        Logger.log(name + " destroyed");
    }
    
    @Override
    public void interact(Soldier soldier) {
        soldier.pickUp(this);
    }

    @Override
    public void accept (FieldElement fe)
    {
        fe.meet(this);
    }

    @Override
    public void accept(Movable m) {
        m.interact(this);
    }

    @Override
    public void move (BasicField f) {
        f.memorize(this);
        currentField = f;
        currentField.meet(this);
    }

}
