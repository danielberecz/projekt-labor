package game;

public interface Nameable {
    
    void setName(String s);
    String getName();

}
