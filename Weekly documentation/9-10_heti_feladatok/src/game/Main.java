package game;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {
    
    public enum Direction {WEST, EAST, SOUTH, NORTH}


    public static void main(String[] args) {

        if (args.length == 0) {
            Logger.forceLog("Syntax error - mapfile wasn't specified");
            return;
        }

        Controller controller = new Controller(args[0]);
        controller.buildModel();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        // Reading the commands until there is, and passing it to the controller
        String command;
        try {

            while ((command = in.readLine()) != null) {
                controller.execute(command.toLowerCase());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
