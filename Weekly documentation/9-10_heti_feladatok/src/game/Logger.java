package game;


public class Logger {

    // We don't want to log before the map was completely built
    private static boolean enabled = false;

    public static void enableLogging() {
        enabled = true;
    }

    /**
     * Writes message to the standard output, if the Logger is enabled
     * @param message The text to write to the standard output
     */
    public static void log(String message) {
        if (enabled) {
            System.out.println(message);
        }
    }

    /**
     * Writes message to the standard output
     * @param message The text to write to the standard output
     */
    public static void forceLog(String message) {
        System.out.println(message);
    }



}
