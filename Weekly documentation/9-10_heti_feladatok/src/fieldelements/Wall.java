package fieldelements;

import movables.Bullet;
import game.Logger;
import movables.Replicator;
import movables.Soldier;

public class Wall extends FieldElement {
    
    static int i = 1;

    public Wall(BasicField bf) {
        ownField = bf;
        ownField.giveFieldElement(this);
        name = "Wall" + i++;
    }



    @Override
    public void meet(Bullet b) {

        Logger.log(b.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");

        b.destroy();

    }

    @Override
    public void meet(Soldier s) {
        Logger.log(s.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
    }

    @Override
    public void meet(Replicator r) {
        Logger.log(r.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
    }
    
}
