package fieldelements;

import movables.Bullet;
import movables.Soldier;
import game.*;

public class SpecialWall extends Wall {

    
    private Stargate gate;

    private int id;
    
    public SpecialWall(BasicField bf, int id) {
        super(bf);
        this.id = id;
        name = "SpecialWall" + id;
        gate = null;
    }

    public int getId() {
        return id;
    }

    public void closeGate () {

        gate = null;

    }

    public void openGate(char colour, char direction){
        Bullet.Colour c;
        switch (colour) {
            case 'b':
                c = Bullet.Colour.BLUE;
                break;
            case 'y':
                c = Bullet.Colour.YELLOW;
                break;
            case 'r':
                c = Bullet.Colour.RED;
                break;
            case 'g':
                c = Bullet.Colour.GREEN;
                break;
            default:
                Logger.log("Color " + colour + " doesn't exist");
                return;
        }
        Main.Direction d;
        switch (direction) {
            case 'n':
                d = Main.Direction.NORTH;
                break;
            case 'w':
                d = Main.Direction.WEST;
                break;
            case 's':
                d = Main.Direction.SOUTH;
                break;
            case 'e':
                d = Main.Direction.EAST;
                break;
            default:
                Logger.log("Direction " + direction + " doesn't exist");
                return;
        }
        gate = new Stargate(c, ownField.getNeighbour(d), this);
    }

    @Override
    public void meet(Soldier s) {

        Logger.log(s.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");

        if(gate != null)
        {

            gate.stepIn(s);

        }

    }

    @Override
    public void meet(Bullet b) {

        Logger.log(b.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
        BasicField associatedField = b.getCurrentField();
        b.destroy();

        gate = new Stargate(b.getColour(), associatedField, this);


    }

    public int getX() {
        return ownField.getX();
    }

    public int getY() {
        return ownField.getY();
    }
}
