package fieldelements;

import movables.Bullet.Colour;
import game.Logger;
import game.Nameable;
import movables.Soldier;

public class Stargate implements Nameable {

    private static Wormhole wormhole;
    private Colour colour;
    private BasicField associatedField;
    private SpecialWall mySpecialWall;
    private String name;
    private static int id = 1;

    static {
        wormhole = new Wormhole();
    }

    public String getName() {
        return name;
    }

    public void setName(String s) {
        name = s;
    }
    
    public Stargate(Colour c, BasicField f, SpecialWall sw) {
        name = "Stargate" + id++;
        colour = c;
        associatedField = f;
        mySpecialWall = sw;
        wormhole.openStargate(colour, this);

        Logger.log(name + " opened: " + c.toString() + " on " + sw.getName() + " (" + sw.getX() + ", " + sw.getY() + ")");

    }
    
    public void stepIn(Soldier ONeill) {

        wormhole.travelTrough(ONeill, colour);

    }

    public void stepOut(Soldier ONeill) {
        associatedField.meet(ONeill);
    }
    
    public void close() {

        mySpecialWall.closeGate();

    }
    
}
