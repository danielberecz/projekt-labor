package fieldelements;

import movables.Bullet;
import game.Logger;
import movables.Replicator;
import movables.Soldier;

public class Door extends FieldElement {

    enum Accessibility {CLOSED, OPEN}
    private Accessibility state = Accessibility.CLOSED;

    private int id;

    
    public Door(BasicField bf, int id) {
        this.id = id;
        name = "Door" + id;
        ownField = bf;
        ownField.giveFieldElement(this);
    }

    public int getId() {
        return id;
    }


    public void open() {

        state = Accessibility.OPEN;

        Logger.log(name + " opened (" + ownField.getX() + ", " + ownField.getY() + ")");

    }
    public void close() {

        state = Accessibility.CLOSED;
        ownField.destroyMovables();

        Logger.log(name + " closed (" + ownField.getX() + ", " + ownField.getY() + ")");

    }


    @Override
    public void meet(Replicator r) {
        if (state == Accessibility.OPEN) {
            r.move(ownField);
        }
        else {
            Logger.log(r.getName() + " collided: " + this.name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
        }
    }

    @Override
    public void meet(Soldier s) {


        if(state == Accessibility.OPEN)
        {
            s.move(ownField);
        }
        else
        {
            Logger.log(s.getName() + " collided: " + this.name + " (" + ownField.getX() + ", " + ownField.getY() + ")");

        }
    }

    @Override
    public void meet(Bullet b) {

        if(state == Accessibility.OPEN) {

            b.move(ownField);

        }
        else {

            b.destroy();

        }

    }

}
