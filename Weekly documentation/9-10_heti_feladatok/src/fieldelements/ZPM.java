package fieldelements;

import game.Model;
import movables.Bullet;
import movables.Replicator;
import movables.Soldier;

public class ZPM extends FieldElement {
    
    private static int id = 1;

    private Model model;

    public ZPM(BasicField bf, Model m) {
        name = "ZPM" + id++;
        ownField = bf;
        ownField.giveFieldElement(this);

        model = m;
    }

    @Override
    public void meet(Soldier soldier) {

        soldier.move(ownField);

        soldier.pickUp(this);

        model.ZPMCollected(soldier);

    }

    @Override
    public void meet(Replicator replicator) {

        replicator.move(ownField);

    }

    @Override
    public void meet(Bullet b)
    {
        b.move(ownField);
    }

}
