//
// P. - pit
// ..- emptyField
// D1 - door+id
// _1 - switch+id
// Z. - ZPM
// B5 - Box+weight
// W. - wall
// S1 - special wall+ID
// O. - Oneill spawnpoint
// J. - Jaffa spawnpoint
// R1 - Replicator spawnpoint+ReplicatorID

//PALYA X-Y MERETE
x:10
y:10

//PALYA ELRENDEZESE
W. W. W. W. W. W. W. W. W. W.
W. O. _1 .. .. .. .. .. B5 W.
W. W. W. W. D1 W. W. W. W. W.
W. .. .. .. .. P. .. .. .. W.
W. .. P. P. P. P. .. P. .. W.
W. .. P. Z. .. .. .. P. .. W.
W. .. P. .. .. .. .. P. .. W.
W. .. P. P. P. P. P. P. .. W.
W. .. .. .. .. .. .. .. .. W.
W. W. W. W. W. W. W. W. W. W.

//MERLEGEK HATARA ES RAJTA LEVO DOBOZOK(TOMEGE) <switch>:<threshold>;<weight_1>,<weightht_2>,...,<weight_n>
_1:5;1,2,2
_2:10;5,8
_3:20

//Az összes objektum aminek van id-je egy hashmapben meg lesz feleltetve a programnon belüli objektummal (pl: Si = SpecialWall_j)
