﻿SelfMovable
Absztrakt ősosztály melyből azon osztályok származnak, amelyek önmaguktól (más osztályoktól függetlenül) is képesek mozogni
Ősosztályok
Movable
Interfészek
nincs
Attribútumok
#currentField: Ősosztályból örökölt tagváltozó
#facing:Direction Ebben a változóban tároljuk, hogy melyik irányba (Enum: North/South/East/West) néz az ezt az ősosztályt öröklő osztály egy példánya jelenleg. Ez határozza meg a lövedék és a lépés irányát
Metódusok:
+getDirection():Direction A facing attribútum értékét adja meg
+rotate():void Ezzel a függvénnyel lehet megváltoztatni, hogy melyik irányba nézzen az objektum