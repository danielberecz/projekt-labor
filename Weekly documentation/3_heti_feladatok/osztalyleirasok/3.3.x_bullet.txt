﻿Bullet
A golyót megvalósító osztály. Ennek az osztálynak a felellősége, hogy ha egy speciális fallal kerül kapcsolatba, akkor ott megfelelő színű portált hozzon létre.
Ősosztályok
Movable --> SelfMovable
Interfészek
nincs
Attribútumok
-currentField:BasicField az ősosztályból örökölt tagváltozó
-facing:Direction Az ősosztályból örökölt tagváltozó
-colour:Colour A lövedék színét határozza meg. Típusa eg enum, amely lehetséges értékei 'orange' illetve 'blue'
Metódus
+accept(m: Movable):void, örökölt függvény A visitor patternek megfelelően meghívja a bejövő paraméter visit függvényét önmagával, hogy kapcsolatba léphessen a mezőn lévő Movable-ből származő objektumokkal
+accept(bf:BasicField):void, örökölt függvény A visitor patternek megfelelően meghívja a bejövő paraméter visit függvényét önmagával, hogy kapcsolatba léphessen a mezőn lévő FieldElementből származó objektumokkal
+getColour():Colour A colour tagváltozó értékét adja meg