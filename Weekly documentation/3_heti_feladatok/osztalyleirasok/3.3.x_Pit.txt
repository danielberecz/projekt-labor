Pit
A j�t�kban szerepl� szakad�kok megval�s�t�sa. Felel�ss�ge, hogy ha a dobozzal vagy O'Neill ezredessel interakci�ba ker�l, akkor megsemmis�tse �ket. A l�ved�k elrep�l felette.

�soszt�lyok:
FieldElement

Interf�szek:
Nincs

Attrib�tumok:
-ownField:Basicfield az �soszt�lyb�l �r�k�lt tagv�ltoz�.
 
Met�dusok:
+void visit(Box box): A dobozzal val� interakci� f�ggv�nye, a dobozt megsemmis�ti.
+void visit(Soldier soldier): O'Neill ezredessel val� interakci� f�ggv�nye, meg�li az ezredest.
+void visit(Bullet bullet): Ez a f�ggv�ny sz�l a l�ved�knek, hogy oda mozoghat az adott Fieldre. 
