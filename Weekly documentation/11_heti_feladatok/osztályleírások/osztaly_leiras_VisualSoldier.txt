\subsection{VisualSoldier}
	\begin{itemize}
	\item Felelősség\newline
	A játékosok megjelenítését támogató osztály. A VisualSoldierFactory osztály belső osztálya.
    
	\item Ősosztályok\newline
	Drawable

	\item Interfészek\newline
	Nincsenek

	\item Attribútumok\newline
	\comment{Milyen attribútumai vannak}
		\begin{itemize}
            \item[] \underline{#zindex:} int: az elem "z" koordinátája, azaz melyik réteg része
            \item[] drawing: String: a kép elérési útja
       \end{itemize}
	\item Metódusok\newline
		\begin{itemize}
            \item[] +VisualPit(Pit): konstruktor, meghívja az ősosztály konstruktorát az adott elemekkel
        \end{itemize}
	\end{itemize}