\select@language {magyar} \contentsline {chapter}{\numberline {3}Anal\IeC {\'\i }zis modell kidolgoz\IeC {\'a}sa 1}{10}{chapter.3}
\select@language {magyar} \contentsline {section}{\numberline {3.1}Objektum katal\IeC {\'o}gus}{10}{section.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.1}O'Neill ezredes}{10}{subsection.3.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.2}Sima mez\IeC {\H o}}{10}{subsection.3.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.3}Szakad\IeC {\'e}k}{10}{subsection.3.1.3}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.4}M\IeC {\'e}rleg}{10}{subsection.3.1.4}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.5}Ajt\IeC {\'o}}{10}{subsection.3.1.5}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.6}Norm\IeC {\'a}l fal}{10}{subsection.3.1.6}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.7}Speci\IeC {\'a}lis fal}{11}{subsection.3.1.7}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.8}F\IeC {\'e}regj\IeC {\'a}rat}{11}{subsection.3.1.8}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.9}Csillagkapu}{11}{subsection.3.1.9}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.10}L\IeC {\"o}ved\IeC {\'e}k}{11}{subsection.3.1.10}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.11}Doboz}{11}{subsection.3.1.11}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1.12}ZPM}{11}{subsection.3.1.12}
\select@language {magyar} \contentsline {section}{\numberline {3.2}Statikus strukt\IeC {\'u}ra diagramok}{12}{section.3.2}
\select@language {magyar} \contentsline {section}{\numberline {3.3}Oszt\IeC {\'a}lyok le\IeC {\'\i }r\IeC {\'a}sa}{13}{section.3.3}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.1}\textbf {FieldElement}}{13}{subsection.3.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.2}\textbf {Basic Field}}{13}{subsection.3.3.2}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.3}\textbf {ZPM}}{14}{subsection.3.3.3}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.4}\textbf {Pit}}{14}{subsection.3.3.4}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.5}\textbf {Door}}{15}{subsection.3.3.5}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.6}\textbf {Switch}}{15}{subsection.3.3.6}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.7}\textbf {Wall}}{15}{subsection.3.3.7}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.8}\textbf {SpecialWall}}{16}{subsection.3.3.8}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.9}\textbf {Wormhole}}{16}{subsection.3.3.9}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.10}\textbf {Stargate}}{17}{subsection.3.3.10}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.11}\textbf {Movable}}{17}{subsection.3.3.11}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.12}\textbf {Box}}{18}{subsection.3.3.12}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.13}\textbf {SelfMovable}}{18}{subsection.3.3.13}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.14}\textbf {Soldier}}{19}{subsection.3.3.14}
\select@language {magyar} \contentsline {subsection}{\numberline {3.3.15}\textbf {Bullet}}{19}{subsection.3.3.15}
\select@language {magyar} \contentsline {section}{\numberline {3.4}Szekvencia diagramok}{21}{section.3.4}
\select@language {magyar} \contentsline {section}{\numberline {3.5}State-chartok}{39}{section.3.5}
\select@language {magyar} \contentsline {section}{\numberline {3.6}Napl\IeC {\'o}}{41}{section.3.6}
\select@language {magyar} \contentsline {chapter}{\numberline {4}Anal\IeC {\'\i }zis modell kidolgoz\IeC {\'a}sa 2}{43}{chapter.4}
\select@language {magyar} \contentsline {section}{\numberline {4.1}Objektum katal\IeC {\'o}gus}{43}{section.4.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.1}O'Neill ezredes}{43}{subsection.4.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.2}Sima mez\IeC {\H o}}{43}{subsection.4.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.3}Szakad\IeC {\'e}k}{43}{subsection.4.1.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.4}M\IeC {\'e}rleg}{43}{subsection.4.1.4}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.5}Ajt\IeC {\'o}}{43}{subsection.4.1.5}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.6}Norm\IeC {\'a}l fal}{43}{subsection.4.1.6}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.7}Speci\IeC {\'a}lis fal}{44}{subsection.4.1.7}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.8}F\IeC {\'e}regj\IeC {\'a}rat}{44}{subsection.4.1.8}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.9}Csillagkapu}{44}{subsection.4.1.9}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.10}L\IeC {\"o}ved\IeC {\'e}k}{44}{subsection.4.1.10}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.11}Doboz}{44}{subsection.4.1.11}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.12}ZPM}{44}{subsection.4.1.12}
\select@language {magyar} \contentsline {section}{\numberline {4.2}Statikus strukt\IeC {\'u}ra diagramok}{45}{section.4.2}
\select@language {magyar} \contentsline {section}{\numberline {4.3}Oszt\IeC {\'a}lyok le\IeC {\'\i }r\IeC {\'a}sa}{46}{section.4.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.1}\textbf {FieldElement}}{46}{subsection.4.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.2}\textbf {Basic Field}}{46}{subsection.4.3.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.3}\textbf {ZPM}}{47}{subsection.4.3.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.4}\textbf {Pit}}{47}{subsection.4.3.4}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.5}\textbf {Door}}{48}{subsection.4.3.5}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.6}\textbf {Switch}}{48}{subsection.4.3.6}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.7}\textbf {Wall}}{49}{subsection.4.3.7}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.8}\textbf {SpecialWall}}{49}{subsection.4.3.8}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.9}\textbf {Wormhole}}{49}{subsection.4.3.9}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.10}\textbf {Stargate}}{50}{subsection.4.3.10}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.11}\textbf {Movable}}{50}{subsection.4.3.11}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.12}\textbf {Box}}{51}{subsection.4.3.12}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.13}\textbf {SelfMovable}}{52}{subsection.4.3.13}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.14}\textbf {Soldier}}{52}{subsection.4.3.14}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3.15}\textbf {Bullet}}{53}{subsection.4.3.15}
\select@language {magyar} \contentsline {section}{\numberline {4.4}Szekvencia diagramok}{54}{section.4.4}
\select@language {magyar} \contentsline {section}{\numberline {4.5}State-chartok}{64}{section.4.5}
\select@language {magyar} \contentsline {section}{\numberline {4.6}Napl\IeC {\'o}}{65}{section.4.6}
\select@language {magyar} \contentsline {chapter}{\numberline {5}Szkeleton tervez\IeC {\'e}se}{66}{chapter.5}
\select@language {magyar} \contentsline {section}{\numberline {5.1}A szkeleton modell val\IeC {\'o}s\IeC {\'a}gos use-case-ei}{66}{section.5.1}
\select@language {magyar} \contentsline {subsection}{\numberline {5.1.1}Use-case diagram}{66}{subsection.5.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {5.1.2}Use-case le\IeC {\'\i }r\IeC {\'a}sok}{67}{subsection.5.1.2}
\select@language {magyar} \contentsline {section}{\numberline {5.2}A szkeleton kezel\IeC {\H o}i fel\IeC {\"u}let\IeC {\'e}nek terve, dial\IeC {\'o}gusok}{71}{section.5.2}
\select@language {magyar} \contentsline {section}{\numberline {5.3}Szekvencia diagramok a bels\IeC {\H o} m\IeC {\H u}k\IeC {\"o}d\IeC {\'e}sre}{73}{section.5.3}
\select@language {magyar} \contentsline {section}{\numberline {5.4}Kommunik\IeC {\'a}ci\IeC {\'o}s diagramok}{87}{section.5.4}
\select@language {magyar} \contentsline {section}{\numberline {5.5}Napl\IeC {\'o}}{96}{section.5.5}
\select@language {magyar} \contentsline {chapter}{\numberline {6}Szkeleton bead\IeC {\'a}s}{97}{chapter.6}
\select@language {magyar} \contentsline {section}{\numberline {6.1}Kommunik\IeC {\'a}ci\IeC {\'o}s diagramok}{97}{section.6.1}
\select@language {magyar} \contentsline {section}{\numberline {6.2}Ford\IeC {\'\i }t\IeC {\'a}si \IeC {\'e}s futtat\IeC {\'a}si \IeC {\'u}tmutat\IeC {\'o}}{107}{section.6.2}
\select@language {magyar} \contentsline {subsection}{\numberline {6.2.1}F\IeC {\'a}jllista}{107}{subsection.6.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {6.2.2}Ford\IeC {\'\i }t\IeC {\'a}s}{108}{subsection.6.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {6.2.3}Futtat\IeC {\'a}s}{108}{subsection.6.2.3}
\select@language {magyar} \contentsline {section}{\numberline {6.3}\IeC {\'E}rt\IeC {\'e}kel\IeC {\'e}s}{109}{section.6.3}
\select@language {magyar} \contentsline {section}{\numberline {6.4}Napl\IeC {\'o}}{109}{section.6.4}
\select@language {magyar} \contentsline {chapter}{\numberline {7}Protot\IeC {\'\i }pus koncepci\IeC {\'o}ja}{110}{chapter.7}
\select@language {magyar} \contentsline {section}{\numberline {7.1}V\IeC {\'a}ltoz\IeC {\'a}sok a specifik\IeC {\'a}ci\IeC {\'o}ban}{110}{section.7.1}
\select@language {magyar} \contentsline {subsection}{\numberline {7.1.1}Megv\IeC {\'a}ltozott oszt\IeC {\'a}lydiagram}{110}{subsection.7.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {7.1.2}Megv\IeC {\'a}ltozott \IeC {\'e}s \IeC {\'u}j szekvenciadiagramok}{111}{subsection.7.1.2}
\select@language {magyar} \contentsline {section}{\numberline {7.2}Protot\IeC {\'\i }pus interface-defin\IeC {\'\i }ci\IeC {\'o}ja}{114}{section.7.2}
\select@language {magyar} \contentsline {subsection}{\numberline {7.2.1}Az interf\IeC {\'e}sz \IeC {\'a}ltal\IeC {\'a}nos le\IeC {\'\i }r\IeC {\'a}sa}{114}{subsection.7.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {7.2.2}Bemeneti nyelv}{114}{subsection.7.2.2}
\select@language {magyar} \contentsline {section}{\numberline {7.3}\IeC {\"O}sszes r\IeC {\'e}szletes use-case}{118}{section.7.3}
\select@language {magyar} \contentsline {section}{\numberline {7.4}Tesztel\IeC {\'e}si terv}{120}{section.7.4}
\select@language {magyar} \contentsline {section}{\numberline {7.5}Tesztel\IeC {\'e}st t\IeC {\'a}mogat\IeC {\'o} seg\IeC {\'e}d- \IeC {\'e}s ford\IeC {\'\i }t\IeC {\'o}programok specifik\IeC {\'a}l\IeC {\'a}sa}{124}{section.7.5}
\select@language {magyar} \contentsline {section}{\numberline {7.6}Napl\IeC {\'o}}{124}{section.7.6}
\select@language {magyar} \contentsline {chapter}{\numberline {8}R\IeC {\'e}szletes tervek}{126}{chapter.8}
\select@language {magyar} \contentsline {section}{\numberline {8.1}Oszt\IeC {\'a}lyok \IeC {\'e}s met\IeC {\'o}dusok tervei}{126}{section.8.1}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.1}BasicField}{126}{subsection.8.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.2}Door}{126}{subsection.8.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.3}FieldElement}{127}{subsection.8.1.3}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.4}Pit}{127}{subsection.8.1.4}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.5}Switch}{128}{subsection.8.1.5}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.6}Wall}{128}{subsection.8.1.6}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.7}ZPM}{129}{subsection.8.1.7}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.8}Game}{129}{subsection.8.1.8}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.9}Movable}{129}{subsection.8.1.9}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.10}SelfMovable}{130}{subsection.8.1.10}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.11}Box}{131}{subsection.8.1.11}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.12}Soldier}{131}{subsection.8.1.12}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.13}Bullet}{132}{subsection.8.1.13}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.14}Replicator}{133}{subsection.8.1.14}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.15}SpecialWall}{133}{subsection.8.1.15}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.16}Stargate}{134}{subsection.8.1.16}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1.17}Wormhole}{134}{subsection.8.1.17}
\select@language {magyar} \contentsline {section}{\numberline {8.2}A tesztek r\IeC {\'e}szletes tervei, le\IeC {\'\i }r\IeC {\'a}suk a teszt nyelv\IeC {\'e}n}{136}{section.8.2}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.1}Bullet meet Replicator (or Replicator meet Bullet)}{136}{subsection.8.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.2}Bullet meet Wall}{136}{subsection.8.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.3}Oneill picks up Box, then tries to pick up an other (2 in 1)}{137}{subsection.8.2.3}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.4}Oneill puts down Box}{138}{subsection.8.2.4}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.5}Oneill tries to put down box, although he doesn't have one}{138}{subsection.8.2.5}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.6}Oneill picks up ZPM (and the ZPM disapper after pickup)}{139}{subsection.8.2.6}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.7}Oneill shoots blue (new:) and creates blue stargate}{140}{subsection.8.2.7}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.8}Oneill shoots yellow (new:) and creates yellow stargate}{140}{subsection.8.2.8}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.9}Oneill creates Wormhole}{141}{subsection.8.2.9}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.10}Oneill hasn't got enough wieght, but with box OK}{141}{subsection.8.2.10}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.11}Replicator and Pit}{142}{subsection.8.2.11}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.12}ZPM appear after 2 fetched}{143}{subsection.8.2.12}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.13}O\IeC {\textquoteright }Neill and Jaffa\IeC {\textquoteright }s wormhole}{144}{subsection.8.2.13}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.14}O\IeC {\textquoteright }Neill and Replicator}{144}{subsection.8.2.14}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.15}O\IeC {\textquoteright }Neill and Stargate without Wormhole}{145}{subsection.8.2.15}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.16}O\IeC {\textquoteright }Neill and Wall}{145}{subsection.8.2.16}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.17}O\IeC {\textquoteright }Neill and closed Door}{146}{subsection.8.2.17}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.18}O\IeC {\textquoteright }Neill steps}{146}{subsection.8.2.18}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.19}O\IeC {\textquoteright }Neill and his Wormhole}{147}{subsection.8.2.19}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.20}O\IeC {\textquoteright }Neill and open door}{148}{subsection.8.2.20}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.21}O\IeC {\textquoteright }Neill and specWall without Stargate}{148}{subsection.8.2.21}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.22}O\IeC {\textquoteright }Neill steps into Pit}{149}{subsection.8.2.22}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.23}O\IeC {\textquoteright }Neill steps on Switch}{149}{subsection.8.2.23}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.24}Delete everything in Pit}{150}{subsection.8.2.24}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.25}Replikator and Box}{150}{subsection.8.2.25}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.26}Replikator steps on Switch}{151}{subsection.8.2.26}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.27}Replikator and ZPM}{152}{subsection.8.2.27}
\select@language {magyar} \contentsline {subsection}{\numberline {8.2.28}Bullet and open stargate}{152}{subsection.8.2.28}
\select@language {magyar} \contentsline {section}{\numberline {8.3}A tesztel\IeC {\'e}st t\IeC {\'a}mogat\IeC {\'o} programok tervei}{153}{section.8.3}
\select@language {magyar} \contentsline {section}{\numberline {8.4}Napl\IeC {\'o}}{153}{section.8.4}
\select@language {magyar} \contentsline {chapter}{\numberline {10}Protot\IeC {\'\i }pus bead\IeC {\'a}sa}{154}{chapter.10}
\select@language {magyar} \contentsline {section}{\numberline {10.1}Ford\IeC {\'\i }t\IeC {\'a}si \IeC {\'e}s futtat\IeC {\'a}si \IeC {\'u}tmutat\IeC {\'o}}{154}{section.10.1}
\select@language {magyar} \contentsline {subsection}{\numberline {10.1.1}F\IeC {\'a}jllista}{154}{subsection.10.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {10.1.2}Ford\IeC {\'\i }t\IeC {\'a}s}{154}{subsection.10.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {10.1.3}Futtat\IeC {\'a}s}{155}{subsection.10.1.3}
\select@language {magyar} \contentsline {section}{\numberline {10.2}Tesztek jegyz\IeC {\H o}k\IeC {\"o}nyvei}{156}{section.10.2}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.1}ONeill and SpecialWall without Stargate}{156}{subsection.10.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.2}ONaill and Stargate without Wormhole}{156}{subsection.10.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.3}ONeill and Wall}{156}{subsection.10.2.3}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.4}ONeill hasn't got enough weight but with box ok}{156}{subsection.10.2.4}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.5}ONeill picks up Box then tries to pick up another}{156}{subsection.10.2.5}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.6}ONeill picks up a ZPM}{156}{subsection.10.2.6}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.7}ONeill puts down a Box}{157}{subsection.10.2.7}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.8}ONeill shoots blue and open a blue Stargate}{157}{subsection.10.2.8}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.9}ONeill creates Wormhole}{157}{subsection.10.2.9}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.10}Oneill shoots yellow and creates yellow stargate}{157}{subsection.10.2.10}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.11}Oneill steps}{158}{subsection.10.2.11}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.12}Oneill steps into Pit}{158}{subsection.10.2.12}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.13}Oneill steps on Swtich}{158}{subsection.10.2.13}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.14}Oneill tries to put down box although he doesn't have one}{159}{subsection.10.2.14}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.15}Replicator and Box}{159}{subsection.10.2.15}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.16}Replicator and Pit}{159}{subsection.10.2.16}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.17}Replicator and ZPM}{159}{subsection.10.2.17}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.18}Replicator steps on Switch}{159}{subsection.10.2.18}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.19}ZPM appear after 2 fetched}{159}{subsection.10.2.19}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.20}Bullet and open stargate}{160}{subsection.10.2.20}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.21}Bullet meets Replicator}{160}{subsection.10.2.21}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.22}Bullet meets Wall}{160}{subsection.10.2.22}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.23}Delete everything in pit}{161}{subsection.10.2.23}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.24}ONeill and closed Door}{161}{subsection.10.2.24}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.25}ONeill and Wormhole}{161}{subsection.10.2.25}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.26}ONeill and Jaffas Wormhole}{161}{subsection.10.2.26}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.27}ONeill and open Door}{161}{subsection.10.2.27}
\select@language {magyar} \contentsline {subsection}{\numberline {10.2.28}ONeill and Replicator}{162}{subsection.10.2.28}
\select@language {magyar} \contentsline {section}{\numberline {10.3}\IeC {\'E}rt\IeC {\'e}kel\IeC {\'e}s}{163}{section.10.3}
\select@language {magyar} \contentsline {section}{\numberline {10.4}Napl\IeC {\'o}}{163}{section.10.4}
\select@language {magyar} \contentsline {chapter}{\numberline {11}Grafikus fel\IeC {\"u}let specifik\IeC {\'a}ci\IeC {\'o}ja}{164}{chapter.11}
\select@language {magyar} \contentsline {section}{\numberline {11.1}A grafikus interf\IeC {\'e}sz}{164}{section.11.1}
\select@language {magyar} \contentsline {section}{\numberline {11.2}A grafikus rendszer architekt\IeC {\'u}r\IeC {\'a}ja}{166}{section.11.2}
\select@language {magyar} \contentsline {subsection}{\numberline {11.2.1}A fel\IeC {\"u}let m\IeC {\H u}k\IeC {\"o}d\IeC {\'e}si elve}{166}{subsection.11.2.1}
\select@language {magyar} \contentsline {section}{\numberline {11.3}A grafikus objektumok felsorol\IeC {\'a}sa}{168}{section.11.3}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.1}Drawable}{168}{subsection.11.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.2}View}{168}{subsection.11.3.2}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.3}VisualBasicField}{169}{subsection.11.3.3}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.4}VisualBox}{170}{subsection.11.3.4}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.5}VisualPit}{170}{subsection.11.3.5}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.6}VisualReplicator}{171}{subsection.11.3.6}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.7}VisualSpecialWall}{171}{subsection.11.3.7}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.8}VisualWall}{171}{subsection.11.3.8}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.9}VisualZPM}{172}{subsection.11.3.9}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.10}VisualDoor}{172}{subsection.11.3.10}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.11}VisualDoorFactory}{173}{subsection.11.3.11}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.12}VisualSoldier}{173}{subsection.11.3.12}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.13}VisualSoldierFactory}{173}{subsection.11.3.13}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.14}VisualStargate}{174}{subsection.11.3.14}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.15}VisualStargateFactory}{174}{subsection.11.3.15}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.16}VisualSwitch}{175}{subsection.11.3.16}
\select@language {magyar} \contentsline {subsection}{\numberline {11.3.17}VisualSwitchFactory}{175}{subsection.11.3.17}
\select@language {magyar} \contentsline {section}{\numberline {11.4}Kapcsolat az alkalmaz\IeC {\'o}i rendszerrel}{176}{section.11.4}
\select@language {magyar} \contentsline {section}{\numberline {11.5}Napl\IeC {\'o}}{181}{section.11.5}
\select@language {magyar} \contentsline {chapter}{\numberline {13}Grafikus fel\IeC {\"u}let specifik\IeC {\'a}ci\IeC {\'o}ja}{182}{chapter.13}
\select@language {magyar} \contentsline {section}{\numberline {13.1}Ford\IeC {\'\i }t\IeC {\'a}si \IeC {\'e}s futtat\IeC {\'a}si \IeC {\'u}tmutat\IeC {\'o}}{182}{section.13.1}
\select@language {magyar} \contentsline {subsection}{\numberline {13.1.1}F\IeC {\'a}jllista}{182}{subsection.13.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {13.1.2}Ford\IeC {\'\i }t\IeC {\'a}s}{183}{subsection.13.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {13.1.3}Futtat\IeC {\'a}s}{183}{subsection.13.1.3}
\select@language {magyar} \contentsline {section}{\numberline {13.2}\IeC {\'E}rt\IeC {\'e}kel\IeC {\'e}s}{184}{section.13.2}
\select@language {magyar} \contentsline {section}{\numberline {13.3}Napl\IeC {\'o}}{184}{section.13.3}
\select@language {magyar} \contentsline {chapter}{\numberline {14}\IeC {\"O}sszefoglal\IeC {\'a}s}{185}{chapter.14}
\select@language {magyar} \contentsline {section}{\numberline {14.1}Projekt \IeC {\"o}sszegz\IeC {\'e}s}{185}{section.14.1}
