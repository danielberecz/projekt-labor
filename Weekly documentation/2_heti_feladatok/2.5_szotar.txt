﻿O'Neill ezredes:	A játék fõhõse, az egyetlen irányítható karakter.
ZPM:	Zero Point Modul, egy nagy energiaforrás. A labirintusan helyezkednek el elszórtan.
játék célja:	Minél több ZPM-et összegyûjteni a játék során.
csillagkapu:	A féregjárat vég-, illetve kiinduló pontja. Összesen kettõ létezhet egyszerre, két különbözõ színnel azonosíthatóak. 
labirintus:	Falakkal szegélyezett útvesztõ, melyben a ZPM-ek találhatók. 
féregjárat:	Átjáró két csillagkapu között. Térbeli ugrást eredményez. Az ezredes át tud haladni rajta, és tárgyat is tud vinni magával. 
fegyver:	Csillagkapu létrehozására alkalmas eszköz. 
lövedék:	Létezik sárga és kék lövedék, melyet a fegyverbõl lehet kilõni. 
speciális fal:	Ha ezt eltalálja egy lövedék, akkor ott annak megfelelõ színû csillagkapu nyílik.
doboz:	A labirintusban található tárgyak egyike. Elszórtan helyezkednek el. Az ezredes által mozgatható, ha az közvetlenül elõtte helyezhetõ el. 
ajtó:	A labirintusban található tárgy. Falként viselkednek, ha zárt állapotban vannak. 
mérleg:	Minden ajtóhoz tartozik egy. Nem mozgatható. Ha rá súlyt helyezünk, a hozzá tartozó ajtó kinyílik, és mindaddig nyitva marad, míg a súly a mérleget nyomja. Ha a súly lekerül a mérlegrõl, az ajtó visszazáródik. 
súly:	Mérlegre helyezhetõ, alatta egy dobozt, vagy magát az ezredest értjük. 
szakadék:	A labirintus egy olyan mezõje, melyen az ezredes nem tud áthaladni. Lövedék lõhetõ felette. Akármi beleesik, megsemmisül. 
játék vége:	Ha az ezredes a szakadékba esik vagy ha az összes ZPM-t összegyûjtötte. 	
