package fieldelements;

import game.Main.Direction;
import game.Nameable;
import movables.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BasicField extends FieldElement implements Nameable {

    private List<FieldElement> fieldElementList = new ArrayList<>();
    private List<Movable> movableList = new ArrayList<>();
    private Map<Direction, BasicField> neighbours = new HashMap<>();

    private int x;
    private int y;

    public BasicField(int i, int j) {
        x = i;
        y = j;
        name = ("BasicField" + Integer.toString(i) + Integer.toString(j));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public void tryMoving(SelfMovable sM, Direction d) {
        sM.accept(neighbours.get(d));
    }

    public void memorize(Movable m) {
        movableList.add(m);
    }

    public void forget(Movable m) {
        movableList.remove(m);
        leave(m);
    }

    public void forget(FieldElement fe) {
        fieldElementList.remove(fe);
    }

    public void interactMovables() {
        for (int i = movableList.size() - 1; i >= 0; i--) {
            Movable outer = movableList.get(i);
            for (int j = i - 1; j >= 0; j--) {
                outer.accept(movableList.get(j));
            }
            if (i > movableList.size()) {
                i = movableList.size();
            }
        }
    }

    public void requestBoxPlacement(Soldier s) {
        s.putBox(this);
    }

    public void destroyMovables() {
        for (int i = 0; i < movableList.size(); i++) {
            movableList.get(i).destroy();
        }
    }

    @Override
    public void meet(Soldier s) {
        if (fieldElementList.isEmpty()) {
            s.move(this);
        } else {
            for (int i = fieldElementList.size() - 1; i >= 0; i--) {
                fieldElementList.get(i).meet(s);
            }
        }
    }

    @Override
    public void meet(Replicator r) {
        if (fieldElementList.isEmpty()) {
            r.move(this);
        } else {
            for (int i = fieldElementList.size() - 1; i >= 0; i--) {
                fieldElementList.get(i).meet(r);
            }
        }

    }

    @Override
    public void meet(Box b) {
        for (int i = fieldElementList.size() - 1; i >= 0; i--) {
            fieldElementList.get(i).meet(b);
        }
    }

    @Override
    public void meet(Bullet b) {
        if (fieldElementList.isEmpty()) {
            b.move(this);
        } else {
            for (int i = fieldElementList.size() - 1; i >= 0; i--) {
                fieldElementList.get(i).meet(b);
            }
        }
        for (int i = movableList.size() - 1; i >= 0; i--) {
            movableList.get(i).interact(b);
        }
    }

    @Override
    public void leave(Movable m) {
        for (FieldElement fe : fieldElementList) {
            fe.leave(m);
        }
    }

    public void giveNeighbour(Direction d, BasicField bf) {
        neighbours.put(d, bf);
    }

    public BasicField getNeighbour(Direction d) {
        return neighbours.get(d);
    }

    public void giveFieldElement(FieldElement fe) {
        fieldElementList.add(fe);
    }


    public boolean isEmpty() {
        if (fieldElementList.isEmpty() && movableList.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
