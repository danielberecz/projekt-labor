package fieldelements;

import game.Logger;
import movables.*;

public class Switch extends FieldElement {

    private Door myDoor;
    private int weightOnMe;
    private int threshold;
    private int id;

    public Switch(BasicField bf, int id) {
        ownField = bf;
        ownField.giveFieldElement(this);
        myDoor = null;
        weightOnMe = 0;
        threshold = 0;
        this.id = id;
        name = "Switch" + id;
    }

    public BasicField getCurrentField() {
        return ownField;
    }

    public int getId() {
        return id;
    }

    public boolean isPressed() {
        return weightOnMe >= threshold;
    }

    public void setDoor(Door door) {
        myDoor = door;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public void press(int weight) {
        if (weight == 0) {
            return;
        }
        weightOnMe += weight;

        Logger.log(name + " weight changed to: " + weightOnMe);

        if (weightOnMe >= threshold) {
            Logger.log(name + " state changed to: down");
            if (myDoor != null) {
                myDoor.open();
            }
        }
    }

    public void release(int weight) {
        if (weight == 0) {
            return;
        }
        weightOnMe -= weight;

        Logger.log(name + " weight changed to: " + weightOnMe);

        if (weightOnMe < threshold) {
            Logger.log(name + " state changed to: up");
            myDoor.close();
        }
    }


    @Override
    public void meet(Soldier s) {
        s.move(ownField);
        press(s.getWeight());
    }

    @Override
    public void meet(Replicator replicator) {
        replicator.move(ownField);
        press(replicator.getWeight());
    }

    @Override
    public void meet(Box b) {
        press(b.getWeight());
    }

    @Override
    public void meet(Bullet b) {
        b.move(ownField);
    }

    @Override
    public void leave(Movable m) {
        release(m.getWeight());
    }

}
