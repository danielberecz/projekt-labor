package fieldelements;

import game.Logger;
import game.Main;
import game.Nameable;
import movables.Bullet.Colour;
import movables.Soldier;

public class Stargate implements Nameable {

    private static Wormhole wormhole;
    private Colour colour;
    private Main.Direction direction;
    private BasicField associatedField;
    private SpecialWall mySpecialWall;
    private String name;
    private static int id = 1;

    static {
        wormhole = new Wormhole();
    }

    public String getName() {
        return name;
    }

    public void setName(String s) {
        name = s;
    }

    public Stargate(Colour c, Main.Direction d, BasicField f, SpecialWall sw) {
        name = "Stargate" + id++;
        colour = c;
        direction = d;
        associatedField = f;
        mySpecialWall = sw;
        wormhole.openStargate(colour, this);

        Logger.log(name + " opened: " + c.toString() + " on " + sw.getName() + " (" + sw.getX() + ", " + sw.getY() + ")");
    }

    public void stepIn(Soldier ONeill) {
        wormhole.travelTrough(ONeill, colour);
    }

    public void stepOut(Soldier ONeill) {
        ONeill.rotate(direction);
        associatedField.meet(ONeill);
    }

    public void close() {
        wormhole.closeGate(this);
        mySpecialWall.closeGate();
    }

    public int getX() {
        return mySpecialWall.getX();
    }

    public int getY() {
        return mySpecialWall.getY();
    }

    public Colour getColour() {
        return colour;
    }

    public BasicField getAssociatedField() {
        return associatedField;
    }

    public Main.Direction getDirection() {
        return direction;
    }
}
