package fieldelements;

import game.Nameable;
import movables.Bullet;
import movables.Bullet.Colour;
import movables.Soldier;

import java.util.HashMap;
import java.util.Map;

public class Wormhole implements Nameable {

    private static int id = 1;
    private Map<Colour, Stargate> gates = new HashMap<Bullet.Colour, Stargate>();
    private String name;

    public Wormhole() {
        name = "Wormhole_" + id++;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String s) {
        name = s;
    }

    public void openStargate(Colour colour, Stargate stargate) {
        if (gates.get(colour) != null)
            gates.get(colour).close();

        gates.put(colour, stargate);
    }

    public void travelTrough(Soldier ONeill, Colour sgColour) {
        Stargate gate;
        switch (sgColour) {
            case BLUE:
                gate = gates.get(Colour.YELLOW);
                break;
            case YELLOW:
                gate = gates.get(Colour.BLUE);
                break;
            case RED:
                gate = gates.get(Colour.GREEN);
                break;
            case GREEN:
                gate = gates.get(Colour.RED);
                break;
            default:
                gate = null;
        }

        if (gate != null)
            gate.stepOut(ONeill);

    }

    public void closeGate(Stargate stargate) {
        gates.remove(stargate.getColour());
    }
}
