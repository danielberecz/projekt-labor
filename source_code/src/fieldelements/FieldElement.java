package fieldelements;

import game.Nameable;
import movables.*;

public abstract class FieldElement implements Nameable {

    String name;

    protected BasicField ownField;

    public void meet(Soldier soldier) {
    }

    public void meet(Box box) {
    }

    public void meet(Bullet bullet) {
    }

    public void meet(Replicator replicator) {
    }

    public void leave(Movable m) {
    }

    public String getName() {
        return name;
    }

    public void setName(String s) {
        name = s;
    }

    public int getX() {
        return ownField.getX();
    }

    public int getY() {
        return ownField.getY();
    }
}
