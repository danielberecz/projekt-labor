package fieldelements;

import game.Logger;
import movables.Box;
import movables.Bullet;
import movables.Replicator;
import movables.Soldier;

public class Pit extends FieldElement {

    static int id = 1;
    private boolean destroyed = false;

    public Pit(BasicField bf) {
        ownField = bf;
        name = "Pit" + id++;
        ownField.giveFieldElement(this);
    }

    @Override
    public void meet(Soldier soldier) {
        Logger.log(soldier.getName() + " fell into: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
        soldier.move(ownField);
        soldier.destroy();
    }

    @Override
    public void meet(Box box) {
        Logger.log(box.getName() + " fell into: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
        box.move(ownField);
        box.destroy();
    }

    @Override
    public void meet(Bullet b) {
        b.move(ownField);
    }

    @Override
    public void meet(Replicator replicator) {
        replicator.move(ownField);

        Logger.log(replicator.getName() + " fell into: " + this.name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
        replicator.destroy();

        destroy();

        Logger.log(name + " removed (" + ownField.getX() + ", " + ownField.getY() + ")");
    }

    private void destroy() {
        destroyed = true;
        ownField.forget(this);
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
