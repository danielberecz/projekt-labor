package fieldelements;

import game.Logger;
import game.Main;
import movables.Bullet;
import movables.Soldier;

public class SpecialWall extends Wall {

    private Stargate gate;

    private int id;

    public SpecialWall(BasicField bf, int id) {
        super(bf);
        this.id = id;
        name = "SpecialWall" + id;
        gate = null;
    }

    public int getId() {
        return id;
    }

    public void closeGate() {
        gate = null;
    }

    public Stargate openGate(char colourChar, char directionChar) {
        Bullet.Colour bulletColor = Bullet.Colour.getColour(colourChar);

        Main.Direction direction = Main.Direction.getDirection(directionChar);
        Main.Direction oppositeDirection = direction.getOpposite();

        gate = new Stargate(bulletColor, oppositeDirection, ownField.getNeighbour(direction), this);
        return gate;
    }

    @Override
    public void meet(Soldier s) {
        Logger.log(s.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");

        if (gate != null && gate.getAssociatedField() == s.getCurrentField()) {
            gate.stepIn(s);
        }
    }

    @Override
    public void meet(Bullet b) {
        Logger.log(b.getName() + " collided: " + name + " (" + ownField.getX() + ", " + ownField.getY() + ")");
        BasicField associatedField = b.getCurrentField();
        b.destroy();
        if (gate != null) {
            gate.close();
        }
        gate = new Stargate(b.getColour(), b.getDirection().getOpposite(), associatedField, this);
    }

    public Stargate getStargate() {
        return gate;
    }

    public int getX() {
        return ownField.getX();
    }

    public int getY() {
        return ownField.getY();
    }
}
