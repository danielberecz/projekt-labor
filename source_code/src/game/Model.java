package game;

import fieldelements.*;
import movables.Box;
import movables.Bullet;
import movables.Replicator;
import movables.Soldier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {

    private BasicField[][] fields;      // Representation of the map's fields
    private String mapDescription;      // The read description of the map

    // Maps the one character representation of a direction (n, s, e, w) to an enum
    private HashMap<Character, Main.Direction> directionHashMap;
    // Maps the one character representation of a colour (b, y, r, g) to an enum
    private HashMap<Character, Bullet.Colour> colourHashMap;

    private List<Switch> switches = new ArrayList<>();
    private List<Door> doors = new ArrayList<>();
    private List<Pit> pits = new ArrayList<>();
    private List<SpecialWall> specialWalls = new ArrayList<>();
    private List<Stargate> stargates = new ArrayList<>();
    private List<Wall> walls = new ArrayList<>();
    private List<ZPM> zpms = new ArrayList<>();

    private List<Box> boxes = new ArrayList<>();
    private List<Replicator> replicators = new ArrayList<>();

    private Soldier oneill;
    private Soldier jaffa;

    private int zpmLeft = 0;

    public Model(String mapDescriptionFileName) throws IOException {
        mapDescription = new String(Files.readAllBytes(Paths.get(mapDescriptionFileName)));

        directionHashMap = new HashMap<>();
        directionHashMap.put('n', Main.Direction.NORTH);
        directionHashMap.put('w', Main.Direction.WEST);
        directionHashMap.put('s', Main.Direction.SOUTH);
        directionHashMap.put('e', Main.Direction.EAST);

        colourHashMap = new HashMap<>();
        colourHashMap.put('b', Bullet.Colour.BLUE);
        colourHashMap.put('y', Bullet.Colour.YELLOW);
        colourHashMap.put('r', Bullet.Colour.RED);
        colourHashMap.put('g', Bullet.Colour.GREEN);
    }

    /**
     * Builds the model
     */
    public void build() {
        HashMap<Character, Integer> sizes = parseSize();
        int x = sizes.get('x');
        int y = sizes.get('y');

        fields = new BasicField[y][x];
        for (int j = 0; j < y; j++) {
            for (int i = 0; i < x; i++) {
                fields[j][i] = new BasicField(i, j);
            }
        }

        parseMap();

        parseSwitch();

        linkFields();

        parseONeillBox();

        startReplicators();

        // Output Log
        Logger.enableLogging();
        Logger.log("map created");
    }

    private void startReplicators() {
        for (Replicator replicator : replicators) {
            automateReplicator(replicator.getId());
        }
    }

    /**
     * Gets the size of the map from the map description
     *
     * @return A hashmap with two key-value pairs: 'x' and the width of the map, 'y' and the length of the map
     * @use mapDescription
     */
    private HashMap<Character, Integer> parseSize() {
        HashMap<Character, Integer> sizes = new HashMap<>();

        // A regular expression for finding the sizes
        Pattern pattern = Pattern.compile("x:(\\d+)\r*\ny:(\\d+)");

        Matcher matcher = pattern.matcher(mapDescription);

        matcher.find();

        String xStr = matcher.group(1);
        String yStr = matcher.group(2);

        int x = Integer.parseInt(xStr);
        int y = Integer.parseInt(yStr);

        sizes.put('x', x);
        sizes.put('y', y);

        return sizes;
    }

    /**
     * Creates the map with all the objects on it
     *
     * @use mapDescription
     */
    public void parseMap() {
        // Regular expression for finding the different fields in the map description
        Pattern pattern = Pattern.compile("P\\.|\\.\\.|D\\d+|_\\d+|Z\\.|B\\d+|W.|S\\d+|O\\.|J\\.|R\\d+");

        Matcher matcher = pattern.matcher(mapDescription);

        int id;     // The id of an object

        for (BasicField[] row : fields) {
            for (BasicField elem : row) {
                if (!matcher.find())
                    System.out.println("ERROR IN INPUT!");
                String found = matcher.group();
                char f = found.charAt(0);
                switch (f) {
                    case '.':
                        break;
                    case '_':
                        id = Integer.parseInt(found.substring(1));
                        Switch sw = new Switch(elem, id);
                        switches.add(sw);
                        break;
                    case 'B':
                        int weight = Integer.parseInt(found.substring(1));
                        Box box = new Box(elem, weight);
                        boxes.add(box);
                        break;
                    case 'D':
                        id = Integer.parseInt(found.substring(1));
                        Door door = new Door(elem, id);
                        doors.add(door);
                        break;
                    case 'J':
                        jaffa = new Soldier(elem, "Jaffa");
                        break;
                    case 'O':
                        oneill = new Soldier(elem, "ONeill");
                        break;
                    case 'P':
                        Pit pit = new Pit(elem);
                        pits.add(pit);
                        break;
                    case 'R':
                        id = Integer.parseInt(found.substring(1));
                        Replicator replicator = new Replicator(elem, id);
                        replicators.add(replicator);
                        break;
                    case 'S':
                        id = Integer.parseInt(found.substring(1));
                        SpecialWall specialWall = new SpecialWall(elem, id);
                        specialWalls.add(specialWall);
                        break;
                    case 'W':
                        Wall wall = new Wall(elem);
                        walls.add(wall);
                        break;
                    case 'Z':
                        ZPM zpm = new ZPM(elem, this);
                        zpms.add(zpm);
                        zpmLeft++;
                        break;
                }
            }
        }

        // Linking doors and switches together
        outer:
        for (Door d : doors) {
            for (Switch s : switches) {
                int d_idx = d.getId();
                int s_idx = s.getId();

                if (s_idx == d_idx) {
                    s.setDoor(d);
                    continue outer;
                }
            }
        }

    }

    /**
     * Sets the threshold of the switches, and places boxes on them, if it is specified in the map description
     *
     * @use mapDescription
     */
    public void parseSwitch() {
        // Regular expression for parsing the switches.
        // 1st group: the switch ID
        // 2nd group: the threshold of the switch
        // 3rd group: the weights of the boxes placed on the switch (comma seperated list
        Pattern pattern = Pattern.compile("_(\\d+)+:(\\d+);([\\d,]*)");

        Matcher matcher = pattern.matcher(mapDescription);

        while (matcher.find()) {
            int switchId = Integer.parseInt(matcher.group(1));
            int switchThreshold = Integer.parseInt(matcher.group(2));
            String[] boxWeights = matcher.group(3).split(",");

            // Finding the specified switch (based on id), and then adding the boxes to its field
            for (Switch s : switches) {
                if (s.getId() == switchId) {
                    s.setThreshold(switchThreshold);
                    for (String w : boxWeights) {
                        try {
                            int weight = Integer.parseInt(w);
                            Box box = new Box(s.getCurrentField(), weight);
                            boxes.add(box);
                            s.press(box.getWeight());
                        } catch (NumberFormatException e) {
                        }
                    }
                }
            }
        }

    }

    /**
     * Gives ONeill a box in the beginning
     */
    public void parseONeillBox() {
        Pattern pattern = Pattern.compile("O:(\\d+)");

        Matcher matcher = pattern.matcher(mapDescription);

        if (matcher.find()) {
            int boxWeight = Integer.parseInt(matcher.group(1));
            try {
                Box box = new Box(oneill.getCurrentField(), boxWeight);
                boxes.add(box);
                oneill.pickUp(box);
            } catch (NullPointerException e) {
            }
        }
    }

    /**
     * Links the Basic fields together, so that each sees its neighbours
     */
    private void linkFields() {
        // Sizes of the map
        int x = fields[0].length;
        int y = fields.length;

        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                try {
                    fields[i][j].giveNeighbour(Main.Direction.NORTH, fields[i - 1][j]);
                } catch (IndexOutOfBoundsException e) {
                }
                try {
                    fields[i][j].giveNeighbour(Main.Direction.WEST, fields[i][j - 1]);
                } catch (IndexOutOfBoundsException e) {
                }
                try {
                    fields[i][j].giveNeighbour(Main.Direction.SOUTH, fields[i + 1][j]);
                } catch (IndexOutOfBoundsException e) {
                }
                try {
                    fields[i][j].giveNeighbour(Main.Direction.EAST, fields[i][j + 1]);
                } catch (IndexOutOfBoundsException e) {
                }
            }
        }
    }

    /**
     * Opens a door
     *
     * @param id the ID of the door to open
     */
    public void openDoor(int id) {
        for (Door door : doors) {
            if (door.getId() == id) {
                door.open();
                return;
            }
        }
        Logger.log("Door with ID: " + id + " doesn't exist");
    }

    /**
     * Opens a stargate
     *
     * @param id    the ID of the door to open
     * @param color the one letter representation of the stargate's colour (r, g, b, y)
     */
    public void openStargate(int id, char color, char direction) {
        for (SpecialWall specialWall : specialWalls) {
            if (specialWall.getId() == id) {
                Stargate gate = specialWall.openGate(color, direction);
                stargates.add(gate);
                return;
            }
        }
        Logger.log("SpecialWall with ID: " + id + " doesn't exist");
    }

    /**
     * Moves a replicator to its current direction
     *
     * @param id the ID of the replicator to move
     */
    public void moveReplicator(int id) {
        for (Replicator replicator : replicators) {
            if (replicator.getId() == id) {
                replicator.startMoving();
                return;
            }
        }
        Logger.log("Replicator with ID: " + id + " doesn't exist");
    }

    /**
     * Automates the moving of a replicator
     *
     * @param id the ID of the replicator whose movement is to be automated
     */
    public void automateReplicator(int id) {
        for (Replicator replicator : replicators) {
            if (replicator.getId() == id) {
                replicator.start();
                return;
            }
        }
        Logger.log("Replicator with ID: " + id + " doesn't exist");
    }

    /**
     * Rotates a replicator to a given direction
     *
     * @param id        the ID of the replicator to rotate
     * @param direction the one character representation of the direction which the replicator should turn to
     */
    public void rotateReplicator(int id, char direction) {
        for (Replicator replicator : replicators) {
            if (replicator.getId() == id) {
                Main.Direction dir = directionHashMap.get(direction);
                replicator.rotate(dir);
                return;
            }
        }
        Logger.log("Replicator with ID: " + id + " doesn't exist");
    }

    public void moveSoldier(char s) {
        Soldier soldier;
        if (s == 'j') {
            soldier = jaffa;
        } else {
            soldier = oneill;
        }

        if (soldier != null) {
            soldier.startMoving();
        } else {
            Logger.log("Soldier doesn't exist");
        }
    }

    public void dropBoxSoldier(char s) {
        Soldier soldier;
        if (s == 'j') {
            soldier = jaffa;
        } else {
            soldier = oneill;
        }
        if (soldier != null) {
            soldier.startPutBox();
        } else {
            Logger.log("Soldier doesn't exist");
        }

    }

    public void pickBoxSoldier(char s) {
        Soldier soldier;
        if (s == 'j') {
            soldier = jaffa;
        } else {
            soldier = oneill;
        }

        if (soldier != null) {
            soldier.startPickBox();
        } else {
            Logger.log("Soldier doesn't exist");
        }
    }

    public void shootSoldier(char s, char colour) {
        Soldier soldier;
        Bullet.Colour c = colourHashMap.get(colour);
        if (s == 'j') {
            soldier = jaffa;
        } else {
            soldier = oneill;
        }

        if (soldier != null) {
            if (c != null) {
                soldier.shoot(c);
            } else {
                Logger.log("Colour " + colour + " doesn't exist");
            }
        } else {
            Logger.log("Soldier doesn't exist");
        }

    }

    public void turnOrMoveSoldier(char s, char direction) {
        Soldier soldier;
        Main.Direction d = directionHashMap.get(direction);
        if (s == 'j') {
            soldier = jaffa;
        } else {
            soldier = oneill;
        }
        if (soldier.getDirection().equals(d))
            moveSoldier(s);
        else
            turnSoldier(s, direction);
    }

    public void turnSoldier(char s, char direction) {
        Soldier soldier;
        Main.Direction d = directionHashMap.get(direction);
        if (s == 'j') {
            soldier = jaffa;
        } else {
            soldier = oneill;
        }

        if (soldier != null) {
            if (d != null) {
                soldier.rotate(d);
            } else {
                Logger.log("Direction " + direction + " doesn't exist");
            }
        } else {
            Logger.log("Soldier doesn't exist");
        }
    }

    public void ZPMCollected(Soldier soldier) {
        zpmLeft--;
        if (soldier.isZPMEven()) {
            createRandomZPM();
        }

        if (zpmLeft == 0) {
            Logger.log("Game over");
        }
    }

    private void createRandomZPM() {
        int randomX;
        int randomY;
        int count = 0;
        do {
            randomX = ThreadLocalRandom.current().nextInt(0, fields.length);
            randomY = ThreadLocalRandom.current().nextInt(0, fields[0].length);
            count++;
            if (count > fields.length * fields[0].length) {
                return;
            }
        } while (!fields[randomX][randomY].isEmpty());

        BasicField f = fields[randomX][randomY];

        ZPM zpm = new ZPM(f, this);
        zpms.add(zpm);

        zpmLeft++;

        Logger.log("New ZPM appeared: " + zpm.getName() + " (" + f.getX() + ", " + f.getY() + ")");
    }

    public BasicField[][] getBasicFields() {
        return fields;
    }

    public List<Door> getDoors() {
        return doors;
    }

    public List<Pit> getPits() {
        for (int i = pits.size() - 1; i >= 0; i--) {
            if (pits.get(i).isDestroyed()) {
                pits.remove(i);
            }
        }
        return pits;
    }

    public List<SpecialWall> getSpecialWalls() {
        return specialWalls;
    }

    public List<Stargate> getStargates() {
        stargates = new ArrayList<>();
        for (SpecialWall specialWall : specialWalls) {

            Stargate stargate = specialWall.getStargate();
            if (stargate != null) {
                stargates.add(stargate);
            }
        }
        return stargates;
    }

    public List<Switch> getSwitches() {
        return switches;
    }

    public List<Wall> getWalls() {
        return walls;
    }

    public List<ZPM> getZPMs() {
        for (int i = zpms.size() - 1; i >= 0; i--) {
            if (zpms.get(i).isCollected()) {
                zpms.remove(i);
            }
        }
        return zpms;
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public List<Replicator> getReplicators() {
        for (int i = replicators.size() - 1; i >= 0; i--) {
            if (replicators.get(i).isDead()) {
                replicators.remove(i);
            }
        }
        return replicators;
    }

    public Soldier getOneill() {
        if (oneill != null && oneill.isDead()) {
            oneill = null;
        }
        return oneill;
    }

    public Soldier getJaffa() {
        if (jaffa != null && jaffa.isDead()) {
            jaffa = null;
        }
        return jaffa;
    }
}
