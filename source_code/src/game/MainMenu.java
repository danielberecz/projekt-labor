package game;

import view.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Dani on 2016.05.17..
 */
public class MainMenu extends JFrame implements ActionListener {
    JButton button;

    private String mapFilename;

    public MainMenu(String arg) {
        super("Main menu");
        mapFilename = arg;
        setSize(new Dimension(300, 300));

        button = new JButton("New Game");
        add(button);

        button.addActionListener(this);

        File folder = new File("maps");
        File[] listOfFiles = folder.listFiles();
        JList<File> list = new JList<>(listOfFiles);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.setVisible(false);
        Controller controller = new Controller(mapFilename);
        controller.buildModel();

        View view = new View(controller.getModel());
        view.setVisible(true);
    }
}
