package game;

import java.io.IOException;


public class Controller {

    private Model model;

    public Controller(String mapDescriptionFileName) {
        try {
            model = new Model(mapDescriptionFileName);
        } catch (IOException e) {
            Logger.forceLog("The specified file for the map was not found");
            System.exit(-1);
        }
    }

    public Model getModel() {
        return model;
    }

    public void buildModel() {
        model.build();
    }

    /**
     * Executes the command
     *
     * @param command the command to execute
     */
    public void execute(String command) {
        String[] parsed = command.split(" -");

        for (int i = 0; i < parsed.length; i++) {
            parsed[i] = parsed[i].trim();
        }

        if (parsed.length < 2) {
            Logger.log(parsed[0] + " Missing option(s)");
            return;
        }

        // Checking for the type of the command, and calling the appropriate function for it
        switch (parsed[0]) {
            case "oneill":
                executeONeill(parsed);
                break;
            case "jaffa":
                executeJaffa(parsed);
                break;
            case "replicator":
                executeReplicator(parsed);
                break;
            case "osg":
                executeOsg(parsed);
                break;
            case "od":
                executeOd(parsed);
                break;
        }
    }

    /**
     * Executing the od command (Open door)
     *
     * @param parsed the parsed input command (Controller.execute() must be called first)
     */
    private void executeOd(String[] parsed) {
        try {
            int id = Integer.parseInt(parsed[1]);
            model.openDoor(id);
        } catch (NumberFormatException e) {
            Logger.log(parsed[0] + " Invalid argument: " + parsed[1]);
        }
    }

    /**
     * Executing the osg command (Open stargate)
     *
     * @param parsed the parsed input command (Controller.execute() must be called first)
     */
    private void executeOsg(String[] parsed) {
        if (parsed.length < 4) {
            Logger.log(parsed[0] + " Missing option(s)");
            return;
        } else {
            try {
                int id = Integer.parseInt(parsed[1]);
                char color = parsed[2].charAt(0);
                char direction = parsed[3].charAt(0);

                model.openStargate(id, color, direction);
            } catch (NumberFormatException e) {
                Logger.log(parsed[0] + " Invalid argument: " + parsed[1]);
            }
        }
    }

    /**
     * Executing the Replicator command (move, turn or automate)
     *
     * @param parsed the parsed input command (Controller.execute() must be called first)
     */
    private void executeReplicator(String[] parsed) {
        try {
            int id = Integer.parseInt(parsed[1]);

            switch (parsed.length) {
                case 3:
                    switch (parsed[2]) {
                        case "m":
                            model.moveReplicator(id);
                            break;
                        case "automate":
                            model.automateReplicator(id);
                            break;
                    }
                    break;
                case 4:
                    char direction = parsed[3].charAt(0);
                    if (parsed[2].charAt(0) == 't') {
                        model.rotateReplicator(id, direction);
                    } else {
                        Logger.log(parsed[0] + " Invalid argument: " + parsed[2]);
                    }
                    break;
                default:
                    Logger.log(parsed[0] + " Invalid number of arguments");
            }
        } catch (NumberFormatException e) {
            Logger.log(parsed[0] + " Invalid argument: " + parsed[1]);
        }
    }

    /**
     * Executing the ONeill command
     *
     * @param parsed the parsed input command (Controller.execute() must be called first)
     */
    private void executeONeill(String[] parsed) {
        executeSoldier(parsed, 'o');
    }

    /**
     * Executing the Jaffa command
     *
     * @param parsed the parsed input command (Controller.execute() must be called first)
     */
    private void executeJaffa(String[] parsed) {
        executeSoldier(parsed, 'j');
    }

    /**
     * Executing the ONeill or Jaffa command (executeONeill or executeJaffa calls this function)
     *
     * @param parsed the parsed input command
     * @param s      the character representation of ONeill ('o') or the Jaffa ('j')
     */
    private void executeSoldier(String[] parsed, char s) {
        switch (parsed.length) {
            case 2:
                switch (parsed[1]) {
                    case "m":
                        model.moveSoldier(s);
                        break;
                    case "p":
                        model.pickBoxSoldier(s);
                        break;
                    case "d":
                        model.dropBoxSoldier(s);
                        break;
                    default:
                        Logger.log(parsed[0] + " Invalid argument: " + parsed[1]);
                }
                break;
            case 3:
                switch (parsed[1]) {
                    case "s":
                        char colour = parsed[2].charAt(0);
                        model.shootSoldier(s, colour);
                        break;
                    case "t":
                        char direction = parsed[2].charAt(0);
                        model.turnSoldier(s, direction);
                        break;
                    default:
                        Logger.log(parsed[0] + " Invalid argument: " + parsed[1]);
                }
                break;
            default:
                Logger.log(parsed[0] + " Invalid number of arguments");
        }

    }
}
