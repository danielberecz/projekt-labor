package game;

import view.MainMenu;
import view.View;


public class Main {

    public enum Direction {
        NORTH, WEST, SOUTH, EAST;

        private Direction opposite;
        private int rotation;

        static {

            EAST.opposite = WEST;
            EAST.rotation = 0;

            SOUTH.opposite = NORTH;
            SOUTH.rotation = 1;

            WEST.opposite = EAST;
            WEST.rotation = 2;

            NORTH.opposite = SOUTH;
            NORTH.rotation = 3;

        }

        public Direction getOpposite() {
            return opposite;
        }

        static public Direction getDirection(char c) {
            Direction d;
            switch (c) {
                case 'n':
                    d = Main.Direction.NORTH;
                    break;
                case 'w':
                    d = Main.Direction.WEST;
                    break;
                case 's':
                    d = Main.Direction.SOUTH;
                    break;
                case 'e':
                    d = Main.Direction.EAST;
                    break;
                default:
                    Logger.log("Direction " + c + " doesn't exist");
                    return null;
            }
            return d;
        }

        public int getRotation() {
            return rotation;
        }
    }


    public static void main(String[] args) {
        MainMenu mainMenu = new MainMenu();
        mainMenu.setVisible(true);
    }


    public static void StartGame(String s) {
        Controller controller = new Controller(s);
        controller.buildModel();

        View view = new View(controller.getModel());
        view.setVisible(true);
    }
}
