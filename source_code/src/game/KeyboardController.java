package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Dani on 2016.05.01..
 */
public class KeyboardController implements KeyListener {

    Model model;

    public KeyboardController(Model model) {
        this.model = model;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            System.out.println("Right key pressed");
            model.turnOrMoveSoldier('j', 'e');
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            System.out.println("Up key pressed");
            model.turnOrMoveSoldier('j', 'n');
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            System.out.println("Left key pressed");
            model.turnOrMoveSoldier('j', 'w');
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            System.out.println("Down key pressed");
            model.turnOrMoveSoldier('j', 's');
        } else if (e.getKeyCode() == KeyEvent.VK_W) {
            System.out.println("W key pressed");
            model.turnOrMoveSoldier('o', 'n');
        } else if (e.getKeyCode() == KeyEvent.VK_A) {
            System.out.println("A key pressed");
            model.turnOrMoveSoldier('o', 'w');
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
            System.out.println("S key pressed");
            model.turnOrMoveSoldier('o', 's');
        } else if (e.getKeyCode() == KeyEvent.VK_D) {
            System.out.println("D key pressed");
            model.turnOrMoveSoldier('o', 'e');
        } else if (e.getKeyCode() == KeyEvent.VK_H) {
            System.out.println("h key pressed");
            model.shootSoldier('o', 'b');
        } else if (e.getKeyCode() == KeyEvent.VK_K) {
            System.out.println("k key pressed");
            model.shootSoldier('o', 'y');
        } else if (e.getKeyCode() == KeyEvent.VK_U) {
            System.out.println("u key pressed");
            model.pickBoxSoldier('o');
        } else if (e.getKeyCode() == KeyEvent.VK_J) {
            System.out.println("j key pressed");
            model.dropBoxSoldier('o');
        } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD3) {
            System.out.println("num 3 key pressed");
            model.shootSoldier('j', 'r');
        } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD1) {
            System.out.println("num 1 key pressed");
            model.shootSoldier('j', 'g');
        } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD5) {
            System.out.println("num 5 key pressed");
            model.pickBoxSoldier('j');
        } else if (e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
            System.out.println("num 2 key pressed");
            model.dropBoxSoldier('j');
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
