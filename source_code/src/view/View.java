package view;

import fieldelements.*;
import game.KeyboardController;
import game.Model;
import movables.Box;
import movables.Replicator;
import movables.Soldier;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class View extends JFrame {

    Model model;

    List<Drawable> drawableList = new ArrayList<>();

    JLayeredPane layeredPane;

    // Classes from the model
    BasicField[][] basicFields;
    List<Door> doors;
    private List<Pit> pits;
    private List<SpecialWall> specialWalls;
    private List<Stargate> stargates = new ArrayList<>();
    private List<Switch> switches;
    private List<Wall> walls;
    private Soldier oneill;
    private Soldier jaffa;

    private List<Soldier> soldiers;
    private List<ZPM> zpms;
    private List<Box> boxes;
    private List<Replicator> replicators;

    private List<VisualDoorFactory.VisualDoor> visualDoors = new ArrayList<>();
    private List<VisualSwitchFactory.VisualSwitch> visualSwitches = new ArrayList<>();
    private List<VisualZPM> visualZPMs = new ArrayList<>();
    private List<VisualBox> visualBoxes = new ArrayList<>();
    private List<VisualReplicator> visualReplicators = new ArrayList<>();
    private List<VisualSoldierFactory.VisualSoldier> visualSoldiers = new ArrayList<>();
    private List<VisualStargateFactory.VisualStargate> visualStargates = new ArrayList<>();
    private List<VisualPit> visualPits = new ArrayList<>();

    public View(Model model) {
        super("Game");

        this.model = model;

        setSize(new Dimension(600, 600));
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.setLayout(null);

        layeredPane = getLayeredPane();
        layeredPane.setLayout(null);

        initialize();

        int delay = 20; //milliseconds
        ActionListener pullTask = evt -> pull();
        new Timer(delay, pullTask).start();

        addKeyListener(new KeyboardController(model));
    }

    public void initialize() {
        basicFields = model.getBasicFields();
        for (BasicField[] row : basicFields) {
            for (BasicField field : row) {
                addBasicField(field);
            }
        }

        specialWalls = model.getSpecialWalls();
        for (SpecialWall specialWall : specialWalls) {
            addSpecialWall(specialWall);
        }

        walls = model.getWalls();
        for (Wall wall : walls) {
            addWall(wall);
        }

        validate();
        repaint();

    }

    public void pull() {
        // getLayeredPane().removeAll();
        JLayeredPane layeredPane = getLayeredPane();

        for (VisualPit visualPit : visualPits) {
            layeredPane.remove(visualPit);
        }

        // Removing changeable components
        for (VisualDoorFactory.VisualDoor visualDoor : visualDoors) {
            layeredPane.remove(visualDoor);
        }

        for (VisualSwitchFactory.VisualSwitch visualSwitch : visualSwitches) {
            layeredPane.remove(visualSwitch);
        }

        for (VisualStargateFactory.VisualStargate visualStargate : visualStargates) {
            layeredPane.remove(visualStargate);
        }

        for (VisualZPM visualZPM : visualZPMs) {
            layeredPane.remove(visualZPM);
        }

        for (VisualSoldierFactory.VisualSoldier visualSoldier : visualSoldiers) {
            layeredPane.remove(visualSoldier);
        }

        for (VisualBox visualBox : visualBoxes) {
            layeredPane.remove(visualBox);
        }

        for (VisualReplicator visualReplicator : visualReplicators) {
            layeredPane.remove(visualReplicator);
        }

        // Adding changeable components
        pits = model.getPits();
        for (Pit pit : pits) {
            addPit(pit);
        }

        doors = model.getDoors();
        for (Door door : doors) {
            addDoor(door);
        }

        switches = model.getSwitches();
        for (Switch sw : switches) {
            addSwitch(sw);
        }

        stargates = model.getStargates();
        for (Stargate stargate : stargates) {
            addStargate(stargate);
        }

        zpms = model.getZPMs();
        for (ZPM zpm : zpms) {
            if (!zpm.isCollected()) {
                addZPM(zpm);
            }
        }

        oneill = model.getOneill();
        if (oneill != null) {
            addSoldier(oneill);
        }


        jaffa = model.getJaffa();
        if (jaffa != null) {
            addSoldier(jaffa);
        }

        boxes = model.getBoxes();
        for (Box box : boxes) {
            addBox(box);
        }

        replicators = model.getReplicators();
        for (Replicator replicator : replicators) {
            addReplicator(replicator);
        }

        validate();
        repaint();
    }

    private void addReplicator(Replicator replicator) {
        VisualReplicator visualReplicator = new VisualReplicator(replicator);
        visualReplicators.add(visualReplicator);
        add(visualReplicator);
    }

    private void addBox(Box box) {
        if (!box.isInHand()) {
            VisualBox visualBox = new VisualBox(box);
            visualBoxes.add(visualBox);
            add(visualBox);
        }
    }

    private void addZPM(ZPM zpm) {
        VisualZPM visualZPM = new VisualZPM(zpm);
        visualZPMs.add(visualZPM);
        add(visualZPM);
    }

    private void addSoldier(Soldier oneill) {
        VisualSoldierFactory.VisualSoldier visualSoldier = VisualSoldierFactory.getVisualSoldier(oneill);
        visualSoldiers.add(visualSoldier);
        add(visualSoldier);
    }

    private void addWall(Wall wall) {
        VisualWall visualWall = new VisualWall(wall);
        add(visualWall);
    }

    private void addSwitch(Switch sw) {
        VisualSwitchFactory.VisualSwitch visualSwitch = VisualSwitchFactory.getVisualSwitch(sw);
        visualSwitches.add(visualSwitch);
        add(visualSwitch);
    }

    private void addSpecialWall(SpecialWall specialWall) {
        VisualSpecialWall visualSpecialWall = new VisualSpecialWall(specialWall);
        add(visualSpecialWall);
    }

    private void addPit(Pit pit) {
        VisualPit visualPit = new VisualPit(pit);
        visualPits.add(visualPit);
        add(visualPit);
    }

    private void addDoor(Door door) {
        VisualDoorFactory.VisualDoor visualDoor = VisualDoorFactory.getVisualDoor(door);
        visualDoors.add(visualDoor);
        add(visualDoor);
    }


    public void addBasicField(BasicField basicField) {
        VisualBasicField visualBasicField = new VisualBasicField(basicField);
        add(visualBasicField);
    }

    public void addStargate(Stargate stargate) {
        VisualStargateFactory.VisualStargate visualStargate = VisualStargateFactory.getVisualStargate(stargate);
        visualStargates.add(visualStargate);
        add(visualStargate);
    }

    public void add(Drawable d) {
        layeredPane.add(d, d.getZIndex());
    }

}
