package view;

import fieldelements.Stargate;
import game.Main;
import movables.Bullet;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static javax.imageio.ImageIO.read;

public class VisualStargateFactory {

    private static String drawingBlue = "graphics/bluegate.png";
    private static String drawingYellow = "graphics/yellowgate.png";
    private static String drawingGreen = "graphics/greengate.png";
    private static String drawingRed = "graphics/redgate.png";

    private static BufferedImage blue;
    private static BufferedImage yellow;
    private static BufferedImage green;
    private static BufferedImage red;

    static {
        try {
            blue = read(new File(drawingBlue));
            yellow = read(new File(drawingYellow));
            green = read(new File(drawingGreen));
            red = read(new File(drawingRed));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualStargateFactory() {

    }

    public static VisualStargate getVisualStargate(Stargate stargate) {
        Bullet.Colour c = stargate.getColour();
        Main.Direction direction = stargate.getDirection();
        double rotation = direction.getRotation() * Math.PI / 2;

        AffineTransform transform = new AffineTransform();
        transform.rotate(rotation, blue.getWidth() / 2, blue.getHeight() / 2);
        AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);

        switch (c) {
            case BLUE:
                BufferedImage transformedBlue = op.filter(blue, null);
                return new VisualStargate(new Point(stargate.getX(), stargate.getY()), transformedBlue);
            case YELLOW:
                BufferedImage transformedYellow = op.filter(yellow, null);
                return new VisualStargate(new Point(stargate.getX(), stargate.getY()), transformedYellow);
            case GREEN:
                BufferedImage transformedGreen = op.filter(green, null);
                return new VisualStargate(new Point(stargate.getX(), stargate.getY()), transformedGreen);
            case RED:
                BufferedImage transformedRed = op.filter(red, null);
                return new VisualStargate(new Point(stargate.getX(), stargate.getY()), transformedRed);
            default:
                return null;
        }
    }

    public static class VisualStargate extends Drawable {
        private static int zIndex = 2;

        public VisualStargate(Point coordinates, BufferedImage image) {
            super(new Point((int) coordinates.getX() * width, (int) coordinates.getY() * height), image, zIndex);
        }
    }

}
