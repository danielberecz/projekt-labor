package view;

import fieldelements.Switch;

import java.awt.*;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualSwitchFactory {
    private static String drawingOn = "graphics/onswitch.png";
    private static String drawingOff = "graphics/offswitch.png";

    public VisualSwitchFactory() {

    }

    public static VisualSwitch getVisualSwitch(Switch sw) {
        if (sw.isPressed()) {
            return new VisualSwitch(new Point(sw.getX(), sw.getY()), drawingOn);
        } else {
            return new VisualSwitch(new Point(sw.getX(), sw.getY()), drawingOff);
        }
    }

    public static class VisualSwitch extends Drawable {
        private static int zIndex = 1;

        public VisualSwitch(Point coordinates, String imageFilename) {
            super(new Point((int) coordinates.getX() * width, (int) coordinates.getY() * height), imageFilename, zIndex);
        }
    }
}
