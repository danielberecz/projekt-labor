package view;

import fieldelements.SpecialWall;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualSpecialWall extends Drawable {
    private static int zIndex = 1;
    private static String drawing = "graphics/specwall.png";

    private static BufferedImage specialWallImage;

    static {
        try {
            specialWallImage = ImageIO.read(new File(drawing));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualSpecialWall(SpecialWall specialWall) {
        super(new Point(specialWall.getX() * width, specialWall.getY() * height), specialWallImage, zIndex);
    }
}
