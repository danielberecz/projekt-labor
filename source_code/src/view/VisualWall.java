package view;

import fieldelements.Wall;

import java.awt.*;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualWall extends Drawable {
    private static int zIndex = 1;
    private static String drawing = "graphics/wall.png";

    public VisualWall(Wall wall) {
        super(new Point(wall.getX() * width, wall.getY() * height), drawing, zIndex);
    }
}
