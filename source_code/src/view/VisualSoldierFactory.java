package view;

import game.Main;
import movables.Soldier;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualSoldierFactory {

    private static String drawingONeillNorth = "graphics/oneillnorth.png";
    private static String drawingONeillWest = "graphics/oneillwest.png";
    private static String drawingONeillSouth = "graphics/oneillsouth.png";
    private static String drawingONeillEast = "graphics/oneilleast.png";

    private static String drawingJaffaNorth = "graphics/jaffanorth.png";
    private static String drawingJaffaWest = "graphics/jaffawest.png";
    private static String drawingJaffaSouth = "graphics/jaffasouth.png";
    private static String drawingJaffaEast = "graphics/jaffaeast.png";

    private static BufferedImage ONeillNorthImage;
    private static BufferedImage ONeillWestImage;
    private static BufferedImage ONeillSouthImage;
    private static BufferedImage ONeillEastImage;

    private static BufferedImage JaffaNorthImage;
    private static BufferedImage JaffaWestImage;
    private static BufferedImage JaffaSouthImage;
    private static BufferedImage JaffaEastImage;

    static {
        try {
            ONeillNorthImage = ImageIO.read(new File(drawingONeillNorth));
            ONeillWestImage = ImageIO.read(new File(drawingONeillWest));
            ONeillSouthImage = ImageIO.read(new File(drawingONeillSouth));
            ONeillEastImage = ImageIO.read(new File(drawingONeillEast));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public VisualSoldierFactory() {

    }

    public static VisualSoldier getVisualSoldier(Soldier soldier) {
        Main.Direction d = soldier.getDirection();
        switch (d) {

            case NORTH:
                if (soldier.getName().equals("ONeill"))
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), ONeillNorthImage);
                else
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), drawingJaffaNorth);
            case WEST:
                if (soldier.getName().equals("ONeill"))
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), ONeillWestImage);
                else
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), drawingJaffaWest);
            case SOUTH:
                if (soldier.getName().equals("ONeill"))
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), ONeillSouthImage);
                else
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), drawingJaffaSouth);
            case EAST:
                if (soldier.getName().equals("ONeill"))
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), ONeillEastImage);
                else
                    return new VisualSoldier(new Point(soldier.getX(), soldier.getY()), drawingJaffaEast);
            default:
                return null;
        }
    }

    public static class VisualSoldier extends Drawable {

        private static int zIndex = 4;

        private VisualSoldier(Point coordinates, BufferedImage image) {
            super(new Point((int) coordinates.getX() * width, (int) coordinates.getY() * height), image, zIndex);
        }

        private VisualSoldier(Point coordinates, String imageFilename) {
            super(new Point((int) coordinates.getX() * width, (int) coordinates.getY() * height), imageFilename, zIndex);
        }
    }
}

