package view;

import fieldelements.BasicField;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class VisualBasicField extends Drawable {

    private static int zIndex = 0;
    private static String drawing = "graphics/basicfield.png";

    private static BufferedImage basicFieldImage;

    static {
        try {
            basicFieldImage = ImageIO.read(new File(drawing));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualBasicField(BasicField basicField) {
        super(new Point(basicField.getX() * width, basicField.getY() * height), basicFieldImage, zIndex);
    }
}
