package view;

import fieldelements.Pit;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class VisualPit extends Drawable {
    private static int zIndex = 1;
    private static String drawing = "graphics/pit.png";

    private static BufferedImage pitImage;

    static {
        try {
            pitImage = ImageIO.read(new File(drawing));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualPit(Pit pit) {
        super(new Point(pit.getX() * width, pit.getY() * height), pitImage, zIndex);
    }
}
