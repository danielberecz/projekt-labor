package view;

import fieldelements.Door;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualDoorFactory {
    private static String drawingClosed = "graphics/closeddoor.png";
    private static String drawingOpen = "graphics/opendoor.png";

    private static BufferedImage closedImage;
    private static BufferedImage openImage;

    static {
        try {
            closedImage = ImageIO.read(new File(drawingClosed));
            openImage = ImageIO.read(new File(drawingOpen));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualDoorFactory() {
    }

    public static VisualDoor getVisualDoor(Door door) {
        if (door.isOpen()) {
            return new VisualDoor(new Point(door.getX(), door.getY()), openImage);
        } else {
            return new VisualDoor(new Point(door.getX(), door.getY()), closedImage);
        }
    }

    public static class VisualDoor extends Drawable {
        private static int zIndex = 1;

        public VisualDoor(Point coordinates, BufferedImage image) {
            super(new Point((int) coordinates.getX() * width, (int) coordinates.getY() * height), image, zIndex);
        }
    }
}
