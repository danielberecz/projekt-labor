package view;

import movables.Replicator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualReplicator extends Drawable {
    private static int zIndex = 4;
    private static String drawing = "graphics/replicator.png";

    private static BufferedImage replicatorImage;

    static {
        try {
            replicatorImage = ImageIO.read(new File(drawing));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualReplicator(Replicator replicator) {
        super(new Point(replicator.getX() * width, replicator.getY() * height), replicatorImage, zIndex);
    }
}
