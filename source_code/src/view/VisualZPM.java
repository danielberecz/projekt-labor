package view;

import fieldelements.ZPM;

import java.awt.*;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualZPM extends Drawable {
    private static int zIndex = 3;
    private static String drawing = "graphics/zpm.png";

    public VisualZPM(ZPM zpm) {
        super(new Point(zpm.getX() * width, zpm.getY() * height), drawing, zIndex);
    }
}
