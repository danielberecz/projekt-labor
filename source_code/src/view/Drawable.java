package view;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Drawable extends JComponent {

    protected static int width = 64;
    protected static int height = 64;

    Point coordinates;
    protected static Dimension size = new Dimension(width, height);

    BufferedImage image;

    int zIndex;

    Drawable(Point coordinates, BufferedImage image, int zIndex) {
        this.coordinates = coordinates;
        this.image = image;
        this.zIndex = zIndex;
        setBounds((int) coordinates.getX(), (int) coordinates.getY(), (int) size.getWidth(), (int) size.getHeight());
    }

    Drawable(Point coordinates, String imageFilename, int zIndex) {
        try {
            this.coordinates = coordinates;
            image = ImageIO.read(new File(imageFilename));
            this.zIndex = zIndex;
            setBounds((int) coordinates.getX(), (int) coordinates.getY(), (int) size.getWidth(), (int) size.getHeight());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getZIndex() {
        return new Integer(zIndex);
    }

    @Override
    public int getX() {
        return (int) coordinates.getX();
    }

    @Override
    public int getY() {
        return (int) coordinates.getY();
    }

    public int getWidth() {
        return (int) size.getWidth();
    }

    public int getHeight() {
        return (int) size.getHeight();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width = (int) size.getWidth();
        int height = (int) size.getHeight();
        Image scaled = image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
        g.drawImage(scaled, 0, 0, null);
    }

}
