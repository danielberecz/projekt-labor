package view;

import game.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;


/**
 * Created by mars on 2016.05.17..
 */
public class MainMenu extends JFrame implements ActionListener {
    public MainMenu() {
        super("Main Menu");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        setMinimumSize(new Dimension(300, 200));
        initComponents();
    }

    private JButton SelectButton = new JButton("SELECT");
    File[] listOfFiles;
    JList list;

    private void initComponents() {
        this.setLayout(new BorderLayout());

        SelectButton.setActionCommand("ButtonPress");
        SelectButton.addActionListener(this);

        File folder = new File("maps");
        listOfFiles = folder.listFiles();

        list = new JList(listOfFiles);
        list.setSelectedIndex(0);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(SelectButton);
        panel2.add(new JScrollPane(list));
        add(panel, BorderLayout.SOUTH);
        add(panel2, BorderLayout.NORTH);
    }

    public void actionPerformed(java.awt.event.ActionEvent ae) {
        if (ae.getActionCommand().equals("ButtonPress")) {
            this.setVisible(false);
            Main m = new Main();
            m.StartGame(list.getSelectedValue().toString());
        }
    }
}
