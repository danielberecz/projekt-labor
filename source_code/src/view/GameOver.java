package view;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mars on 2016.05.17..
 */
public class GameOver extends JFrame {
    public GameOver() {
        super("Main Menu");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        setMinimumSize(new Dimension(300, 200));
        initComponents();
    }

    private void initComponents() {
        JLabel gameOverLabel = new JLabel("GAME OVER!");
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(gameOverLabel);
        add(panel, BorderLayout.CENTER);
    }
}
