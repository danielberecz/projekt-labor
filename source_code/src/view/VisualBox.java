package view;

import movables.Box;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dani on 2016.05.01..
 */
public class VisualBox extends Drawable {
    private static int zIndex = 3;
    private static String drawing = "graphics/box.png";

    private static BufferedImage boxImage;

    static {
        try {
            boxImage = ImageIO.read(new File(drawing));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public VisualBox(Box box) {
        super(new Point(box.getX() * width, box.getY() * height), boxImage, zIndex);
    }
}
