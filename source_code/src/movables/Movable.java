package movables;

import fieldelements.BasicField;
import fieldelements.FieldElement;
import game.Nameable;

public abstract class Movable implements Nameable {

    protected BasicField currentField;
    protected int weight;
    protected String name;

    public Movable() {
    }

    public String getName() {
        return name;
    }

    public void setName(String s) {
        name = s;
    }

    public void interact(Soldier s) {
    }

    public void interact(Box b) {
    }

    public void interact(Bullet b) {
    }

    public void interact(Replicator replicator) {
    }

    public void accept(FieldElement fe) {
    }

    public void accept(Movable m) {
    }

    public void destroy() {
    }

    public void move(BasicField bf) {
    }

    public int getWeight() {
        return weight;
    }

    public BasicField getCurrentField() {
        return currentField;
    }

    public int getX() {
        return currentField.getX();
    }

    public int getY() {
        return currentField.getY();
    }
}
