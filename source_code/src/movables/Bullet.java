package movables;

import fieldelements.BasicField;
import fieldelements.FieldElement;
import game.Logger;
import game.Main.Direction;

public class Bullet extends SelfMovable {

    public enum Colour {
        BLUE, YELLOW, RED, GREEN;

        public static Colour getColour(char colourChar) {
            Bullet.Colour bulletColor;
            switch (colourChar) {
                case 'b':
                    bulletColor = Bullet.Colour.BLUE;
                    break;
                case 'y':
                    bulletColor = Bullet.Colour.YELLOW;
                    break;
                case 'r':
                    bulletColor = Bullet.Colour.RED;
                    break;
                case 'g':
                    bulletColor = Bullet.Colour.GREEN;
                    break;
                default:
                    Logger.log("Color " + colourChar + " doesn't exist");
                    bulletColor = null;
            }
            return bulletColor;
        }
    }

    private Colour colour;
    private static int id = 1;

    public Bullet(Colour c, BasicField f, Direction d) {
        name = "Bullet" + id++;
        colour = c;
        currentField = f;
        facing = d;

        while (currentField != null) {
            currentField.tryMoving(this, d);
        }
    }

    public Colour getColour() {
        return colour;
    }

    @Override
    public void accept(FieldElement fe) {
        if (fe == null) {
            currentField = null;
            return;
        }

        fe.meet(this);
    }

    @Override
    public void accept(Movable m) {
        m.interact(this);
    }

    @Override
    public void move(BasicField f) {
        f.memorize(this);
        currentField.forget(this);
        currentField = f;
    }

    @Override
    public void destroy() {
        currentField.forget(this);
        currentField = null;

        Logger.log(name + " exploded");
    }

    @Override
    public void interact(Replicator replicator) {
        Logger.log(this.getName() + " collided: " + replicator.getName() + " (" + currentField.getX() + ", " + currentField.getY() + ")");

        replicator.destroy();
        this.destroy();
    }
}
