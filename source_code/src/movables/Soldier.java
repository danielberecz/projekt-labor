package movables;

import fieldelements.BasicField;
import fieldelements.FieldElement;
import fieldelements.ZPM;
import game.Logger;
import movables.Bullet.Colour;
import view.GameOver;

public class Soldier extends SelfMovable {


    private int zpmCounter = 0;
    private Box carriedBox = null;

    public Soldier(BasicField bf, String name) {
        this.name = name;
        currentField = bf;
        currentField.memorize(this);
        weight = 80;
    }

    public void pickUp(ZPM zpm) {
        zpmCounter++;
        currentField.forget(zpm);

        Logger.log(name + " picked up: ZPM (" + currentField.getX() + ", " + currentField.getY() + ")");
    }

    public void pickUp(Box box) {
        if (carriedBox == null) {
            carriedBox = box;
            currentField.forget(box);
            box.pickedUp(true);
            weight += carriedBox.getWeight();

            Logger.log(name + " picked up: box (" + currentField.getX() + ", " + currentField.getY() + ")");
        }
    }

    public void putBox(BasicField f) {
        if (carriedBox != null) {
            carriedBox.move(f);
            carriedBox.pickedUp(false);
            weight -= carriedBox.getWeight();
            carriedBox = null;

            Logger.log(name + " dropped: box (" + currentField.getX() + ", " + currentField.getY() + ")");
        } else {
            Logger.log(name + " doesn't have a Box to put down");
        }

    }

    public void shoot(Colour c) {
        Logger.log(name + " shot: " + c.toString());

        Bullet b = new Bullet(c, currentField, getDirection());
    }

    @Override
    public void destroy() {
        currentField.forget(this);
        Logger.log(name + " died");
        dead = true;
        GameOver go = new GameOver();
        go.setVisible(true);
    }

    @Override
    public void accept(FieldElement fe) {
        fe.meet(this);
    }

    @Override
    public void accept(Movable m) {
        m.interact(this);
    }

    @Override
    public void interact(Box b) {
        pickUp(b);
    }

    public void startPutBox() {
        currentField.requestBoxPlacement(this);
    }

    public void startPickBox() {
        currentField.interactMovables();
    }

    public boolean isZPMEven() {
        if (zpmCounter % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}
