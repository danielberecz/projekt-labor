package movables;

import fieldelements.BasicField;
import fieldelements.FieldElement;
import game.Main;

import java.util.concurrent.ThreadLocalRandom;

public class Replicator extends SelfMovable implements Runnable {

    private Thread t;
    private String threadName;

    private int id;

    public Replicator(BasicField bf, int id) {
        this.id = id;
        name = "Replicator" + id;
        currentField = bf;
        currentField.memorize(this);
        weight = 0;

        threadName = name + "_Thread";
    }

    public void accept(FieldElement fe) {
        fe.meet(this);
    }

    public void interact(Bullet b) {
        b.interact(this);
    }

    public int getId() {
        return id;
    }

    @Override
    public void run() {
        while (!dead) {
            this.startMoving();
            Main.Direction direction = getRandomDirection();
            this.rotate(direction);
            try {
                t.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void start() {
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }

    private Main.Direction getRandomDirection() {
        Main.Direction direction;
        int randomNumber = ThreadLocalRandom.current().nextInt(1, 5);
        switch (randomNumber) {
            case 0:
                direction = Main.Direction.NORTH;
                break;
            case 1:
                direction = Main.Direction.WEST;
                break;
            case 2:
                direction = Main.Direction.SOUTH;
                break;
            case 3:
                direction = Main.Direction.EAST;
                break;
            default:
                direction = Main.Direction.NORTH;
        }
        return direction;
    }
}
